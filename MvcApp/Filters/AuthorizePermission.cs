﻿using Domain.Models;
using Domain.Services.UserServices;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MvcApp.Filters
{
    public class AuthorizePermission : AuthorizeAttribute
    {
        //[Dependency]
        //public IUserService _userService { get; set; }
        public string Permission;
      
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);            
            
            try
            {
                FormsIdentity id = HttpContext.Current.User.Identity as FormsIdentity;
                FormsAuthenticationTicket ticket = id.Ticket;
                string userData = ticket.UserData;
                string[] permissions = userData.Split(',');

                if (!permissions.Contains(Permission))
                {
                    //var redirectUrl = "~/Error/Unauthorized";
                    //filterContext.RequestContext.HttpContext.Response.Redirect(redirectUrl);
                    filterContext.RequestContext.RouteData.Values["controller"] = "Error";
                    filterContext.Result = new ViewResult { ViewName = "Unauthorized" };
                }
            }
            catch (Exception)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }            

            
        }
    }
}