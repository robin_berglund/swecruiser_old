﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.ViewModels.ProfileViewModels
{
    public class EditDetailsViewModel
    {

        [Required(ErrorMessage = "Du måste ange ett förnamn")]
        [Display(Name = "Förnamn")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Du måste ange ett förnamn")]
        [Display(Name = "Efternamn")]
        public string SurName { get; set; }

        [Display(Name = "Stad")]
        public string City { get; set; }
       
        [Display(Name = "Favoritfordon")]
        public string FavouriteVehicles { get; set; }
        
    }
}