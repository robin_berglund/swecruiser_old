﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels.ProfileViewModels
{
    public class FriendRequestModuleViewModel
    {
        public RequestResponse Response { get; set; }

        public Guid uid { get; set; }
    }
}