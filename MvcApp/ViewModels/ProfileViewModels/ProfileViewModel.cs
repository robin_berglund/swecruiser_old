﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels.ProfileViewModels
{
    public class ProfileViewModel
    {
        public bool IsAccountOwner { get; set; }

        public string ProfileImagePath { get; set; }

        [Display(Name = "Inlägg")]
        public List<Entry> WallEntrys { get; set; }

        [Display(Name = "Namn")]
        public string Name { get; set; }

        [Display(Name = "Stad")]
        public string City { get; set; }

        [Display(Name = "Favoritfordon")]
        public string FavouriteVehicles { get; set; }

        public bool IsFriend { get; set; }

        public Guid UserId { get; set; }

        public RequestResponse FriendStatus { get; set; }

        public List<FriendShip> Friends { get; set; }

    }

}