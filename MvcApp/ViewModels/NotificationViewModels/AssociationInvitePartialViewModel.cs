﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels.NotificationViewModels
{
    public class AssociationInviteRespondViewModel
    {
        public string AssocId { get; set; }

        public string Name { get; set; }

        public int containerId { get; set; }

        public bool IsResponded { get; set; }

        public int NoId { get; set; }
    }
}