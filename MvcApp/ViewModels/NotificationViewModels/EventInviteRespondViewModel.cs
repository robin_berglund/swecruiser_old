﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels.NotificationViewModels
{ 
    public class EventInviteRespondViewModel
    {
        public string EventId { get; set; }

        public string Name { get; set; }

        public int containerId { get; set; }

        public bool IsResponded { get; set; }

        public int NoId { get; set; }
    }
}