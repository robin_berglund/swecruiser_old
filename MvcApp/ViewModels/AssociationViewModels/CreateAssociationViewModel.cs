﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.ViewModels.AssociationViewModels
{
    public class CreateAssociationViewModel
    {
        [Display(Name = "Namn på organisation")]
        [Required(ErrorMessage = "Namn på organisationen måste anges")]
        public string Name { get; set; }

        [Display(Name = "Organisationsnummer för företagsorganisationer")]
        public int Vat { get; set; }

        [Display(Name = "Visningsbild för organisation")]
        public Image BannerImage { get; set; }

        [Display(Name = "Sekretessnivå för organisationssidan")]
        [Required(ErrorMessage = "Sekretessnivå för organisationen måste väljas")]
        public AssociationPrivacy PrivacyType { get; set; }

        [Display(Name = "Organisationstyp")]
        [Required(ErrorMessage = "Kategori för organisationen måste väljas.")]
        public AssociationType Category { get; set; }
    }
}