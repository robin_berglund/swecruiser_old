﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.ViewModels.AssociationViewModels
{
    public class AssociationMembershipsViewModel
    {
        public ProfileInfo UserProfile { get; set; }

        public IList<MembershipViewModel> Memberships { get; set; }
    }

    public class MembershipViewModel
    {
        public AssociationRole Role { get; set; }

        public RequestResponse Response { get; set; }

        public AssociationProfileViewModel Association { get; set; }
    }
}