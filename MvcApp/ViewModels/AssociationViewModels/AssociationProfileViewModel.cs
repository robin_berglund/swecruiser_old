﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.ViewModels.AssociationViewModels
{
    public class AssociationProfileViewModel
    {
        public string Name { get; set; }

        public string id { get; set; }

        public AssociationType Type { get; set; }

        public User OwnerUser { get; set; }

        public Image BannerImage { get; set; }

        public AssociationRole ViewerRole { get; set; }

        public List<AssociationMembership> Members { get; set; }
    }
}