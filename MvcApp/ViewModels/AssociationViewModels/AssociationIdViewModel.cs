﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels.AssociationViewModels
{
    public class AssociationIdViewModel
    {
        public string AssocId { get; set; }
    }
}