﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels.AssociationViewModels
{
    public class AssociationInviteViewModel
    {
        public AssociationInviteViewModel()
        {
            
        }
        public string id { get; set; }
        public List<InviteModel> MembersToinvite { get; set; }
    }
}