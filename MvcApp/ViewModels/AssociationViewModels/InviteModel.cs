﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.ViewModels.AssociationViewModels
{
    public class InviteModel
    {
        public Guid Member { get; set; }
        public ProfileInfo MemberToInvite { get; set; }

        public bool IsInvited { get; set; }
    }
}