﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels.EntryViewModels
{
    public class UserProfileEntryViewModel
    {

        public string Author { get; set; }

        public DateTime DateTime { get; set; }

        public Guid recieverId { get; set; }

        [Required(ErrorMessage = "Du måste skriva något.")]
        [Display(Name = "Meddelande")]
        public string Message { get; set; }

        [Display(Name = "Skriv som privat meddelande")]
        public bool IsPrivate { get; set; }
    }
}