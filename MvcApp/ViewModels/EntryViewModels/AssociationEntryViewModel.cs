﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.ViewModels.EntryViewModels
{
    public class AssociationEntryViewModel
    {
        public string AssocId { get; set; }

        public ProfileInfo Author { get; set; }

        public string Message { get; set; }

        public DateTime PublishDate { get; set; }

        public bool IsPrivate { get; set; }
        public AssociationRole AssociationRole { get; set; }

    }
}