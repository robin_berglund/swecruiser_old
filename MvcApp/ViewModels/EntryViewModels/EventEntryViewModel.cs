﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.ViewModels.EntryViewModels
{
    public class EventEntryViewModel
    {
        public string EventId { get; set; }

        public ProfileInfo Author { get; set; }

        public string Message { get; set; }

        public DateTime PublishDate { get; set; }

        public bool IsPrivate { get; set; }
        public EventRole Role { get; set; }
    }
}