﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "E-postadress måste anges.")]
        [EmailAddress(ErrorMessage = "Angiven e-postadress är inte giltig.")]
        [Display(Name = "E-postadress")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Lösenord måste anges.")]
        [Display(Name = "Lösenord")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Bekräftelse av lösenord måste ifyllas.")]
        [Compare("Password", ErrorMessage = "Lösenorden stämmer inte överrens.")]
        [Display(Name = "Bekräfta lösenord")]
        public string ConfirmPassword { get; set; }

    }
}