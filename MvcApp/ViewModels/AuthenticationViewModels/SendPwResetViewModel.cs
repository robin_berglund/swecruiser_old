﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels.AuthenticationViewModels
{
    public class SendPwResetViewModel
    {
        [Required(ErrorMessage = "Du måste ange en e-postadress")]
        [EmailAddress(ErrorMessage = "Du måste ange en giltig e-postadress")]
        public string Email { get; set; }
    }
}