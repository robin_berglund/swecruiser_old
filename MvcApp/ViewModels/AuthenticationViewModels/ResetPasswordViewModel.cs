﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels.AuthenticationViewModels
{
    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "Du måste fylla i lösenord.")]
        [Display(Name = "Lösenord")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Du måste bekräfta lösenordet.")]
        [Compare("Password", ErrorMessage = "Lösenorden stämmer inte överrens.")]
        [Display(Name = "Bekräfta lösenord")]
        public string ConfirmPassword { get; set; }
    }
}