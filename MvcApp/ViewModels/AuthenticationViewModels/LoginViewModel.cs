﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "E-postadress måste anges.")]
        [EmailAddress(ErrorMessage = "Angiven e-postadress är inte giltig.")]
        [Display(Name = "E-postadress")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Lösenord måste anges.")]
        [Display(Name = "Lösenord")]
        public string Password { get; set; }

        [Display(Name = "Kom ihåg mig på den här datorn")]
        public bool RememberLogin { get; set; }


    }
}