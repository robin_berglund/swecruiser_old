﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels.SearchViewModels
{
    public class SearchResultsDialogueViewModel
    {
        public List<SearchResult> Results { get; set; }
    }
}