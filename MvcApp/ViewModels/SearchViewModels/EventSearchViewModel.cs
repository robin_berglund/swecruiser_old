﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.ViewModels.SearchViewModels
{
    public class EventSearchViewModel
    {
        public List<EventSearchResult> Results { get; set; }
    }
}