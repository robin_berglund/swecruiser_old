﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.ViewModels.SearchViewModels
{
    public class AssociationSearchResultViewModel
    {
        public List<AssociationSearchResult> Results { get; set; }

    }
}