﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcApp.ViewModels.AssociationViewModels;

namespace MvcApp.ViewModels.EventViewModels
{
    public class EventInviteViewModel
    {
        public string EventId { get; set; }
        public List<InviteModel> ParticipantsToinvite { get; set; }
    }
    
}