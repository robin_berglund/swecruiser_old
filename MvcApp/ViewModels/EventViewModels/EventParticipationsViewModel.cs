﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.ViewModels.EventViewModels
{
    public class EventParticipationsViewModel
    {
     
            public ProfileInfo UserProfile { get; set; }

            public IList<ParticipantViewModel> Participants { get; set; }
     
    }
    public class ParticipantViewModel
    {
        public EventRole Role { get; set; }

        public Rsvp Response { get; set; }

        public EventProfileViewModel Event { get; set; }
    }
}