﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.ViewModels.EventViewModels
{
    public class EventProfileViewModel
    {
        public string Name { get; set; }

        public string EventId { get; set; }

        [Display(Name = "Beskrivning")]
        public string Description { get; set; }
        
        [Display(Name = "Börjar")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Slutar")]
        public DateTime EndDate { get; set; }

        public Image BannerImage { get; set; }

        public EventRole ViewerRole { get; set; }

        public EventType Category { get; set; }

        [Display(Name = "Deltagare")]
        public List<EventParticipation> Participants { get; set; }
    }
}