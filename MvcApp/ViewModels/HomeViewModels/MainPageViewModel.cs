﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApp.ViewModels.HomeViewModels
{
    public class MainPageViewModel
    {
        public string ProfileUrl { get; set; }
    }
}