﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.ViewModels.HomeViewModels
{
    public class NotificationsContainerViewModel
    {
        public List<Notification> Notifications { get; set; }

        public List<FriendRequest> FriendRequests { get; set; }

    }
}