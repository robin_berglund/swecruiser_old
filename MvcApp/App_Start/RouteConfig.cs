﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MvcApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Root",
                url: "",
                defaults: new { controller = "Home", action = "MainPage" }
            );
            

            routes.MapRoute(
                name: "Confirmation",
                url: "Confirmaccount/{id}",
                defaults: new { controller = "Authentication", action = "ConfirmAccount", id = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "PasswordReset",
                url: "ResetPassword/{id}",
                defaults: new { controller = "Authentication", action = "ResetPassword", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Login",
                url: "Login",
                defaults: new { controller = "Authentication", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ProfileRoute",
                url: "Profile",
                defaults: new { controller = "Profile", action = "Profile", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "AssocMgmRoute",
                url: "Sallskap-organisationer",
                defaults: new { controller = "Association", action = "AssociationManager" }
            );

            routes.MapRoute(
                name: "AssociationRoute",
                url: "Organisation",
                defaults: new { controller = "Association", action = "AssociationProfile", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "EventMgmRoute",
                url: "Evenemang",
                defaults: new { controller = "Event", action = "EventManager" }
            );

            routes.MapRoute(
                name: "EventRoute",
                url: "Evenemang/{id}",
                defaults: new { controller = "Event", action = "EventProfile", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "MainPage", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "NotFound",
                url: "{*url}",
                defaults: new { controller = "Error", action = "NotFound" }
            );
        }
    }
}
