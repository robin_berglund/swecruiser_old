﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Domain.Models;

namespace MvcApp.InfraStructure
{
    public class ImageHandler
    {
        public string AddImage(HttpServerUtilityBase server, HttpPostedFileBase image, string imageId, string imageCategory)
        {
            try
            {
                var ext = Path.GetExtension(image.FileName);
                string pic = imageId + ext;
                var allowedExtensions = new[] {
                        ".Jpg", ".png", ".jpg", "jpeg"
                    };
                string path = System.IO.Path.Combine(
                                       server.MapPath("~/Images/" + imageCategory), pic);
                path = path.Replace(' ', '_');

                //getting the extension(ex-.jpg)  
                if (allowedExtensions.Contains(ext)) //check what type of extension  
                {
                    image.SaveAs(path);
                }
                return pic;
            }
            catch (Exception)
            {
                throw;
            }
            
        }
    }
}