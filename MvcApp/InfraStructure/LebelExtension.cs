﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Domain.Models;

namespace MvcApp.InfraStructure
{
    public static class LebelExtension
    {

        public static MvcHtmlString EnumLabelFor<TModel>(this HtmlHelper html, TModel value, object htmlAttributes)
        {
            string displayName = EnumHelper<TModel>.GetDisplayValue(value);
            if (string.IsNullOrEmpty(displayName))
            {
                return MvcHtmlString.Empty;
            }
            TagBuilder tagBuilder = new TagBuilder("label");
            var attributes = new RouteValueDictionary(htmlAttributes);
            tagBuilder.MergeAttributes(attributes);
            tagBuilder.SetInnerText(displayName);
            return new MvcHtmlString(tagBuilder.ToString(TagRenderMode.Normal));
        }
    }
}