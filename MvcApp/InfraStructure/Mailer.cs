﻿using Domain.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Models;
using System.Net.Mail;
using System.Configuration;

namespace MvcApp.InfraStructure
{
    public class Mailer : IMailer
    {
        public bool SendConfirmationMail(User user)
        {
            MailMessage message = new MailMessage();
            string noreplyAdress = ConfigurationManager.AppSettings["noReplyEmail"].ToString();
            message.To.Add(user.Email);
            string url = string.Format("{0}://{1}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Authority);
            message.From = new MailAddress(noreplyAdress, "Swecruiser");
            message.IsBodyHtml = true;
            message.Subject = "Bekräfta ditt Swecruiser-konto";
            message.Body = "<h3>Tack för att du skapat ett konto hos oss!</h3></br></br>" +
                "<p>Vänligen klicka på länken nedan för att bekräfta din emailadress.</p>" +
                "<a href='" + url + "/ConfirmAccount/" + "?userID=" + user.UserId + "&confirmationToken=" + user.AccountConfirmationToken + "'>Klicka här för att aktivera.";

            return SendMail(message);
        }

        public bool SendPasswordResetMail(User user)
        {
            MailMessage message = new MailMessage();
            string noreplyAdress = ConfigurationManager.AppSettings["noReplyEmail"].ToString();
            message.To.Add(user.Email);
            string url = string.Format("{0}://{1}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Authority);
            message.From = new MailAddress(noreplyAdress, "Swecruiser");
            message.IsBodyHtml = true;
            message.Subject = "Återställ ditt lösenord";
            message.Body = "<h3>Vi har mottagit en förfrågan om att återställa ditt lösenord.</h3></br></br>" +
                "<p>Vänligen klicka på länken nedan för att komma till sidan där du väljer ditt nya lösenord.</p>" +
                "<a href='" + url + "/ResetPassword/" + "?userID=" + user.UserId + "&pwrToken=" + user.PasswordRecoveryToken + "'>Klicka här för att välja nytt lösenord.";

            return SendMail(message);
        }
        private bool SendMail(MailMessage mail)
        {
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("robintestmail86@gmail.com", "ecxhyfzvgzzzkayr");
            try
            {
                client.Send(mail);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}