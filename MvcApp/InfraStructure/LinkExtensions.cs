﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace MvcApp.InfraStructure
{
    public static class LinkExtensions
    {
        public static MvcHtmlString AssocProfileLink(this HtmlHelper htmlHelper, string linkText, string name, string id, IDictionary<string, object> htmlAttributes)
        {
            var encname = HttpUtility.UrlEncode(name);
            string html = @"<a href=""/Association/AssociationProfile?name=" + encname + "&id=" + id + @""">" + name + "</a>";
            return new MvcHtmlString(html);
        }

        public static MvcHtmlString EventProfileLink(this HtmlHelper htmlHelper, string linkText, string id, string name, IDictionary<string, object> htmlAttributes)
        {
            var encname = HttpUtility.UrlEncode(name);
            string html = @"<a href=""/Evenemang/" + id + @""">" + name + "</a>";
            return new MvcHtmlString(html);
        }
    }
}