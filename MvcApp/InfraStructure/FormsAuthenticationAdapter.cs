﻿using Domain.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using Domain.Models;

namespace MvcApp.InfraStructure
{
    public class FormsAuthenticationAdapter : IAuth
    {
        public void DoAuth(User user, bool persistLogin, string permissions)
        {
            FormsAuthentication.SetAuthCookie(user.Email, persistLogin);
            HttpContext context = HttpContext.Current;
            string[] permissionArray = user.Role.Permissions.Select(x => x.Description).ToArray();
            context.User = new GenericPrincipal(user.Identity, permissionArray);
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                 1,
                 user.Email,                             
                 DateTime.Now,                      
                 DateTime.Now.AddMinutes(30),       
                 false,                              
                 permissions                 
            );

            string encCookie = FormsAuthentication.Encrypt(ticket);
            HttpCookie FormsCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encCookie);
            context.Response.Cookies.Add(FormsCookie);
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}