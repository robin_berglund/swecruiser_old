﻿using Domain.Services.Authentication;
using MvcApp.ViewModels;
using MvcApp.ViewModels.AuthenticationViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MvcApp.Controllers
{
    public class AuthenticationController : Controller
    {
        // GET: Authentication
        IAuthenticationService _service;

        public AuthenticationController(IAuthenticationService service)
        {
            _service = service;
        }


        public ActionResult Login()
        {           
            if(HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("MainPage", "Home");
            }
            return View(); 
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _service.Login(model.Email, model.Password, model.RememberLogin);
                if (result.IsValid)
                {
                    return RedirectToAction("MainPage", "Home");
                }
                ModelState.AddModelError(string.Empty, result.ValidationMessage);
            }
            return View(model);
        }

        public ActionResult Register()
        {
            return View(); 
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.RegisterUser(model.Email, model.Password);
                if (response.IsValid)
                {
                    _service.Login(model.Email, model.Password, false);
                    return RedirectToAction("MainPage", "Home");
                }
                else
                {
                    ModelState.AddModelError("RegisterError", response.ValidationMessage);
                    return View(model);
                }
               
            }
            return View(model);
           
        }


        
        public ActionResult ConfirmAccount(Guid userId, Guid confirmationToken)
        {
            
            var result = _service.ConfirmUser(userId, confirmationToken, true) ;
         
                if (result.IsValid)
                {
                    ViewBag.message = "Ditt konto är nu bekräftat!";
                }
                else
                {
                    ViewBag.message = result.ValidationMessage;
                }
              
                        
            return View();
            
        }

        public ActionResult Logout()
        {
            _service.Logout();
            return RedirectToAction("Login");
        }

        public ActionResult ResetPassword(Guid userId, Guid pwrToken)
        {
            if(User.Identity.IsAuthenticated)
            {
                _service.Logout();
            }            
            var result = _service.ValidatePwResetUrl(userId, pwrToken);

            if (result.IsValid)
            {
                return View();
            }
            else
            {
                ViewBag.message = result.ValidationMessage;
            }
            return View();

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(Guid userId, Guid pwrToken, ResetPasswordViewModel model)
        {

            if (ModelState.IsValid)
            {
                var result = _service.ResetPassword(userId, pwrToken, model.Password);
                if (result.IsValid)
                {
                    ViewBag.message = "Ditt lösenord har ändrats. Du kan nu logga in med ditt nya lösenord!";
                }
                else
                {
                    ModelState.AddModelError("PwResetError", result.ValidationMessage);
                    return View(model);
                }
            }
            return View();

        }

        [HttpGet]
        public ActionResult SendPwReset()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendPwReset(SendPwResetViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _service.SendPasswordResetEmail(model.Email);
                if (result.IsValid)
                {
                    ViewBag.message = "Ett mail för lösenordsåterställning har skickats till din e-postadress!";
                    return PartialView();
                }
                else
                {
                    ModelState.AddModelError("ResetPwError", result.ValidationMessage);
                }
            }
            return PartialView(model);
        }
    }
}