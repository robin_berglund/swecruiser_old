﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Helpers;
using Domain.Models;
using Domain.Services.EventServices;
using Domain.Services.UserServices;
using MvcApp.Filters;
using MvcApp.InfraStructure;
using MvcApp.ViewModels.AssociationViewModels;
using MvcApp.ViewModels.EventViewModels;
using MvcApp.ViewModels.NotificationViewModels;
using MvcApp.ViewModels.SearchViewModels;

namespace MvcApp.Controllers
{
    public class EventController : Controller
    {

        private IEventService _service;
        private IUserService _userService;

        public EventController(IEventService service, IUserService userService)
        {
            _service = service;
            _userService = userService;
        }

        
        // GET: Event
        //public ActionResult ()
        //{
        //    return View();
        //}
        [AllowAnonymous]
        public ActionResult EventManager()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult EventProfile(string id)
        {
            try
            {
                User user;
                var theEvent = _service.GetEventById(int.Parse(Encryption.decrypt(id)));
                EventProfileViewModel model = new EventProfileViewModel
                {
                    Name = theEvent.Name,
                    BannerImage = theEvent.BannerImage,
                    EventId = Encryption.encrypt(theEvent.Id.ToString()),
                    Description = theEvent.Description,
                    Participants = theEvent.Invites.ToList(),
                    EndDate = theEvent.EndTime,
                    StartDate = theEvent.StartTime
      
                };
                if (User.Identity.IsAuthenticated)
                {
                    user = _userService.GetUserByEmail(User.Identity.Name);
                    var currentMember = user != null ? theEvent.Invites.FirstOrDefault(x => x.UserId == user.UserId) : null;
                    model.ViewerRole = currentMember?.Role ?? EventRole.NotAccepted;
                }
                else
                {
                    model.ViewerRole = EventRole.NotAccepted;
                }
                return View(model);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [AuthorizePermission( Permission = "ManageEvent")]
        public ActionResult CreateEvent()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizePermission(Permission = "ManageEvent")]
        public ActionResult CreateEvent(CreateEventViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _service.CreateEvent(model.Description, model.Name, model.StartDate, model.EndDate,
                model.Category, User.Identity.Name);
                if (result.IsValid)
                {
                    ViewBag.message = result.ValidationMessage;
                }
                else
                {
                    ModelState.AddModelError("Error", result.ValidationMessage);
                }
            }

            return PartialView(model);
        }
        

        [AuthorizePermission(Permission = "ManageEvent")]
        public ActionResult SendInvitations()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizePermission(Permission = "ManageEvent")]
        public ActionResult SendInvitations(EventInviteViewModel model)
        {
            return PartialView(model);
        }

        public ActionResult EventSearch(string queryString)
        {
            EventSearchViewModel model = new EventSearchViewModel();
            try
            {
                model.Results = _service.GetEvents(queryString);
            }
            catch (Exception)
            {
                ModelState.AddModelError("Error", "Något gick fel vid sökningen");
            }
            return PartialView(model);
        }

        [Authorize]
        public ActionResult EventParticipations()
        {
            EventParticipationsViewModel model = new EventParticipationsViewModel();
            model.Participants = new List<ParticipantViewModel>();
            List<EventParticipation> participations = _service.GetEventsForUser(User.Identity.Name);
            foreach (var participation in participations)
            {
                model.Participants.Add(new ParticipantViewModel
                {
                    Response = participation.Rsvp,
                    Role = participation.Role,
                    Event = new EventProfileViewModel
                    {
                        Name = participation.Event.Name,
                        Category = participation.Event.Category,
                        EventId = Encryption.encrypt(participation.Event.Id.ToString())
                    }
                });
            }
            return PartialView(model);
        }

        public ActionResult EventInvite(string id)
        {
            EventInviteViewModel model = new EventInviteViewModel { ParticipantsToinvite = new List<InviteModel>() };
            try
            {
                model.EventId = id;
               
                    var friends = _service.GetFriendsViableForInvite(User.Identity.Name, int.Parse(Encryption.decrypt(id)));
                    if (friends != null && friends.Count > 0)
                    {
                        foreach (var friend in friends)
                        {
                            InviteModel m2 = new InviteModel();
                            m2.MemberToInvite = friend;
                            m2.Member = friend.UserId;
                            model.ParticipantsToinvite.Add(m2);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("NoEligible", "Du har inga vänner som inte redan är inbjudna att bjuda in");
                    }

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [AuthorizePermission(Permission = "ManageAssociation")]
        public ActionResult EventInvite(EventInviteViewModel model)
        {
            if (model.ParticipantsToinvite != null && model.ParticipantsToinvite.Count > 0)
            {
                var users = model.ParticipantsToinvite.Where(x => x.IsInvited).Select(x => x.Member).ToList();
                var result = _service.InviteSelection(users, int.Parse(Encryption.decrypt(model.EventId)));
                if (result.IsValid)
                {
                    ViewBag.message = "Inbjudningar skickade!";
                }
            }
            else
            {
                ModelState.AddModelError("NoInvites", "Du måste välja minst en vän att bjuda in.");
            }
            return EventInvite(model.EventId);
        }

        public ActionResult EventInviteRespondView(EventInviteRespondViewModel model)
        {
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult AcceptEventInvitation(EventInviteRespondViewModel model)
        {
            EventParticipation invite = new EventParticipation();
            invite.EventId = int.Parse(Encryption.decrypt(model.EventId));
            invite.Rsvp = Rsvp.Attending;
            var result = _service.RespondToInvite(invite, User.Identity.Name, model.NoId);
            if (result.IsValid)
            {
                ViewBag.message = result.ValidationMessage;
                model.IsResponded = true;
            }
            else
            {
                ModelState.AddModelError("Error", result.ValidationMessage);
            }
            return PartialView("EventInviteRespondView", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DenyEventInvitation(EventInviteRespondViewModel model)
        {
            EventParticipation invite = new EventParticipation();
            invite.EventId = int.Parse(Encryption.decrypt(model.EventId));
            invite.Rsvp = Rsvp.NotResponded;
            var result = _service.RespondToInvite(invite, User.Identity.Name, model.NoId);
            if (result.IsValid)
            {
                ViewBag.message = result.ValidationMessage;
                model.IsResponded = true;
            }
            else
            {
                ModelState.AddModelError("Error", result.ValidationMessage);
            }
            return PartialView("EventInviteRespondView", model);
        }
        public ActionResult UpdateEventProfileImage(string eid)
        {
            EventIdModel model = new EventIdModel();
            model.EventId = eid;

            return PartialView(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult UpdateEventProfileImage(EventIdModel model, HttpPostedFileBase image)
        {
            try
            {
                var theEvent = _service.GetEventById(int.Parse(Encryption.decrypt(model.EventId)));

                if (image != null)
                {
                    var imageHandler = new ImageHandler();
                    string pic = imageHandler.AddImage(Server, image, "eid=" + theEvent.Id, "Events");
                    theEvent.BannerImage = new Image { Path = "/Images/Events/" + pic };
                    _service.EditEvent(theEvent);

                }
                return RedirectToAction("EventProfile", new { name = theEvent.Name, id = model.EventId });
            }
            catch (Exception)
            {
                ModelState.AddModelError("Error", "Något gick fel när bilden skulle sparas.");
            }
            return PartialView(model);
        }

    }
}