﻿using Domain.Services.Authentication;
using Domain.Services.UserServices;
using MvcApp.Filters;
using MvcApp.ViewModels.HomeViewModels;
using MvcApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Models;

namespace MvcApp.Controllers
{
    public class HomeController : Controller
    {

        IUserService _service;
        public HomeController(IUserService service)
        {
            _service = service;
        }
        
        public ActionResult MainPage()
        {
            if (User.Identity.IsAuthenticated)
            {
                MainPageViewModel model = new MainPageViewModel();
                model.ProfileUrl = _service.GetUserByEmail(User.Identity.Name).ProfileInfo.ProfileUrl;
                return View(model);
            }
            return View();
        }

        public ActionResult NotificationsContainer()
        {
            NotificationsContainerViewModel model = new NotificationsContainerViewModel();
            if (User.Identity.IsAuthenticated)
            {
              var user =  _service.GetNotifications(User.Identity.Name);
                model.FriendRequests = user.FriendRequests.ToList() ?? null;
                model.Notifications = user.Notifications.ToList() ?? null;
            }
            return PartialView(model);

        }

        public JsonResult MarkNotificationAsRead(int[] ids)
        {
            if (_service.MarkNotificationsAsRead(ids, User.Identity.Name))
            {
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            
        }

    
    }
}