﻿using Domain.Services.Search;
using MvcApp.ViewModels.SearchViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApp.Controllers
{
    public class SearchController : Controller
    {
        ISearchService _service;

        public SearchController(ISearchService service)
        {
            _service = service;
        }

        public ActionResult SearchResultsDialogue(string queryString)
        {
            SearchResultsDialogueViewModel model = new SearchResultsDialogueViewModel();
            model.Results = new List<Domain.Models.SearchResult>();
            var results = _service.GetTopTenContaining(queryString);
            if (results != null)
            {
                foreach (var res in results)
                {
                    model.Results.Add(res);
                }
                return PartialView(model);
            }
            return PartialView();
        }
        
    }
}