﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Helpers;
using Domain.Models;
using Domain.Services.AssociationServices;
using Domain.Services.FriendServices;
using Domain.Services.PasswordHandler;
using Domain.Services.UserServices;
using MvcApp.Filters;
using MvcApp.InfraStructure;
using MvcApp.ViewModels.AssociationViewModels;
using MvcApp.ViewModels.NotificationViewModels;
using MvcApp.ViewModels.SearchViewModels;

namespace MvcApp.Controllers
{
    public class AssociationController : Controller
    {
        private IAssociationService _service;
        private IUserService _userService;
        private IFriendService _friendService;

        public AssociationController(IAssociationService service, IUserService userService, IFriendService friendService)
        {
            _service = service;
            _userService = userService;
            _friendService = friendService;
        }

        [AuthorizePermission(Permission = "ManageAssociation")]
        public ActionResult CreateAssociation()
        {
            return PartialView();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [AuthorizePermission(Permission = "ManageAssociation")]
        public ActionResult CreateAssociation(CreateAssociationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.GetUserByEmail(User.Identity.Name);
                var result = _service.CreateAssociation(
                    model.Name, model.Vat, 
                    model.BannerImage,
                    user,
                    model.Category,
                    model.PrivacyType);

                if (result.IsValid)
                {
                    ViewBag.message = result.ValidationMessage;
                }
                else
                {
                    ModelState.AddModelError("creationError", result.ValidationMessage);
                }
            }
            return PartialView(model);
        }

        public ActionResult AssociationManager()
        {
            return View();
        }

        public ActionResult AssociationProfile(string name, string id)
        {
            AssociationProfileViewModel model = new AssociationProfileViewModel();
            try
            {
                User user;
                int id1 = int.Parse(Encryption.decrypt(id));
                var association = _service.GetAssociation(name, id1);
                model.Name = HttpUtility.UrlDecode(association.Name);
                model.Members = association.Members.ToList();
                model.BannerImage = association.BannerImage;
                model.Type = association.Category;
                model.OwnerUser = association.AssociationOwner;
                model.id = id;
                if (User.Identity.IsAuthenticated)
                {
                    user = _userService.GetUserByEmail(User.Identity.Name);
                    var currentMember = user != null ? association.Members.FirstOrDefault(x => x.UserId == user.UserId) : null;
                    model.ViewerRole = currentMember?.Role ?? AssociationRole.NoMember;
                }
                else
                {
                    model.ViewerRole = AssociationRole.NoMember;
                }
                
            }
            catch (Exception)
            {
                ModelState.AddModelError("Error", "Ett fel uppstod när sidan skulle laddas");
            }
            return View(model);
        }

        public ActionResult AssociationSearch(string queryString)
        {
            AssociationSearchResultViewModel model = new AssociationSearchResultViewModel();
            try
            {
                model.Results = _service.FindAssociations(queryString);
            }
            catch (Exception)
            {
               ModelState.AddModelError("Error", "Något gick fel vid sökningen");
            }
            return PartialView(model);
        }

        public ActionResult AssociationInvite(string id)
        {
            AssociationInviteViewModel model = new AssociationInviteViewModel { MembersToinvite = new List<InviteModel>()};
            try
            {
                model.id = id;
                var friends = _friendService.GetFriendsForUser(User.Identity.Name);
                if (friends != null)
                {
                    friends = _service.CheckUsersAreMembers(friends, int.Parse(Encryption.decrypt(id)));
                    if (friends != null && friends.Count > 0)
                    {
                        foreach (var friend in friends)
                        {
                            InviteModel m2 = new InviteModel();
                            m2.MemberToInvite = friend;
                            m2.Member = friend.UserId;
                            model.MembersToinvite.Add(m2);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("NoEligible", "Du har inga vänner som inte redan är med i gruppen att bjuda in");
                    }
                    
                }
                else
                {
                    ModelState.AddModelError("NoFriends", "Du har inga vänner att bjuda in.");
                }
                
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [AuthorizePermission(Permission = "ManageAssociation")]
        public ActionResult AssociationInvite(AssociationInviteViewModel model)
        {
            if (model.MembersToinvite != null && model.MembersToinvite.Count > 0)
            {
                var users = model.MembersToinvite.Where(x => x.IsInvited).Select(x => x.Member).ToList();
                var result = _service.InviteSelection(users, int.Parse(Encryption.decrypt(model.id)));
                if (result.IsValid)
                {
                    ViewBag.message = "Inbjudningar skickade!";
                }
            }
            else
            {
                ModelState.AddModelError("NoInvites", "Du måste välja minst en vän att bjuda in.");
            }
            return AssociationInvite(model.id);
        }

        [Authorize]
        public ActionResult AssociationMemberships()
        {
            AssociationMembershipsViewModel model = new AssociationMembershipsViewModel();
            model.Memberships = new List<MembershipViewModel>();
            List<AssociationMembership> memberships = _service.GetAssociationsForUser(User.Identity.Name);
            foreach (var membership in memberships)
            {
              model.Memberships.Add(new MembershipViewModel
              {
                 Response = membership.InviteResponse,
                 Role = membership.Role,
                 Association = new AssociationProfileViewModel
                 {
                     Name = membership.Association.Name,
                     Type = membership.Association.Category,
                     id = Encryption.encrypt(membership.Association.AssocId.ToString())
                 }
              });
            }
            return PartialView(model);
        }

        public ActionResult AssociationInviteRespondView(AssociationInviteRespondViewModel model)
        {
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult AcceptAssociationInvitation(AssociationInviteRespondViewModel model)
        {
            AssociationMembership invite = new AssociationMembership();
            invite.AssocId = int.Parse(Encryption.decrypt(model.AssocId));
            invite.InviteResponse = RequestResponse.Accepted;
            var result = _service.RespondToInvite(invite, User.Identity.Name, model.NoId);
            if (result.IsValid)
            {
                ViewBag.message = result.ValidationMessage;
                model.IsResponded = true;
            }
            else
            {
                ModelState.AddModelError("Error", result.ValidationMessage);
            }
            return PartialView("AssociationInviteRespondView", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DenyAssociationInvitation(AssociationInviteRespondViewModel model)
        {
            AssociationMembership invite = new AssociationMembership();
            invite.AssocId = int.Parse(Encryption.decrypt(model.AssocId));
            invite.InviteResponse = RequestResponse.Denied;
            var result = _service.RespondToInvite(invite, User.Identity.Name, model.NoId);
            if (result.IsValid)
            {
                ViewBag.message = result.ValidationMessage;
                model.IsResponded = true;
            }
            else
            {
                ModelState.AddModelError("Error", result.ValidationMessage);
            }
            return PartialView("AssociationInviteRespondView", model);
        }

        public ActionResult UpdateAssociationProfileImage(string aid)
        {
            AssociationIdViewModel model = new AssociationIdViewModel();
            model.AssocId = aid;

            return PartialView(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult UpdateAssociationProfileImage(AssociationIdViewModel model, HttpPostedFileBase image)
        {
            try
            {
                var association = _service.GetAssociationById(int.Parse(Encryption.decrypt(model.AssocId)));

                if (image != null)
                {
                    var imageHandler = new ImageHandler();
                    string pic = imageHandler.AddImage(Server, image, "aid=" + association.AssocId, "Associations");
                    association.BannerImage = new Image { Path = "/Images/Associations/" + pic };
                    _service.EditAssociation(association);

                }
                return RedirectToAction("AssociationProfile", new { name = association.Name, id = model.AssocId });
            }
            catch (Exception)
            {
                ModelState.AddModelError("Error", "Något gick fel när bilden skulle sparas.");
            }
            return PartialView(model);
        }

    }



}