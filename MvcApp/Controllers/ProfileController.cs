﻿using Domain.Models;
using Domain.Services.UserServices;
using MvcApp.ViewModels.ProfileViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Services.EntryServices;
using Domain.Services.FriendServices;
using MvcApp.InfraStructure;

namespace MvcApp.Controllers
{
    public class ProfileController : Controller
    {
        IUserService _service;
        IFriendService _friendService;
        private IEntryService _entryService;

        public ProfileController(IUserService service, IFriendService friendService, IEntryService entryService)
        {
            _service = service;
            _friendService = friendService;
            _entryService = entryService;
        }
        public ActionResult Profile(Guid? uid)
        {
            User user;
            if (uid != null)
            {
                Guid userId = (Guid)uid;
                user = _service.GetUserById(userId);
            }
            else
            {
                user = _service.GetUserByEmail(User.Identity.Name);
            }
            ProfileViewModel model = new ProfileViewModel();
            
            
            if (user != null)
            {
                model.UserId = user.UserId;
                if (user.ProfileInfo != null)
                {
                    model.City = user.ProfileInfo.City;
                    if (user.ProfileInfo.Firstname != null && user.ProfileInfo.Lastname != null)
                    {
                        model.Name = user.ProfileInfo.Firstname + " " + user.ProfileInfo.Lastname;
                    }                    
                    model.FavouriteVehicles = user.ProfileInfo.FavouriteVehicles;
                    if (user.Email == User.Identity.Name)
                    {
                        model.IsAccountOwner = true;
                    }
                    if (!model.IsAccountOwner && User.Identity.IsAuthenticated)
                    {
                        model.FriendStatus = _friendService.GetFriendStatus(user, User.Identity.Name);
                    }
                    model.ProfileImagePath = user.ProfileInfo.ProfileImage?.Path ;
                }
                else
                {
                    ModelState.AddModelError("NotFound", "Kunde ej ladda användaruppgifter.");
                }
                model.Friends = user.Friends?.ToList();
                if (User.Identity.IsAuthenticated)
                {
                   model.IsFriend = _service.UsersAreFriends(User.Identity.Name, user.UserId); 
                }
                

            }
            else
            {
                ModelState.AddModelError("NotFound", "Kunde ej hitta sidan.");
            }
            return View(model);

        }


        public ActionResult EditDetails()
        {
            EditDetailsViewModel model = new EditDetailsViewModel();
            var user = _service.GetUserByEmail(User.Identity.Name);
            model.SurName = user.ProfileInfo.Lastname;
            model.FirstName = user.ProfileInfo.Firstname ;
            model.City = user.ProfileInfo.City ;
            model.FavouriteVehicles = user.ProfileInfo.FavouriteVehicles;
            return PartialView(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDetails(EditDetailsViewModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        var user = _service.GetUserByEmail(User.Identity.Name);
                        user.ProfileInfo.Lastname = model.SurName;
                        user.ProfileInfo.Firstname = model.FirstName;
                        user.ProfileInfo.City = model.City;
                        user.ProfileInfo.FavouriteVehicles = model.FavouriteVehicles;
                        
                        _service.UpdateUserRecords(user);
                        ViewBag.message = "Ändringar sparade!";
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("UserNotFound", "Kunde inte uppdatera användaruppgifterr. Försök igen senare.");
                    }                    
                }
            }
            else
            {
                ModelState.AddModelError("NotLoggedIn", "Du måste vara inloggad för att ändra uppgifter");
            }
            return PartialView(model);
        }

        public ActionResult UpdateProfileImage()
        {
            return PartialView();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult UpdateProfileImage(HttpPostedFileBase image)
        {
            try
            {
                var user = _service.GetUserByEmail(User.Identity.Name);

                if (image != null)
                {
                    var imageHandler = new ImageHandler();
                    string pic = imageHandler.AddImage(Server, image, "uid=" + user.UserId, "Users");
                    user.ProfileInfo.ProfileImage = new Image { Path = "~/Images/Users/" + pic };
                    _service.UpdateUserRecords(user);
                    
                }
                return RedirectToAction("Profile", new { uid = user.UserId});
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        
        public ActionResult FriendRequestModule()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult FriendRequestModule(Guid uid)
        {
            FriendRequestModuleViewModel model = new FriendRequestModuleViewModel();
            model.uid = uid;
            if (!AddFriend(uid))
            {
                ModelState.AddModelError("Unsuccessful", "Något gick fel vid vänförfrågan, var god försök igen om en stund");                
            }
            else
            {
                model.Response = RequestResponse.NotResponded;
            }
            return PartialView(model);
        }

        
        public bool AddFriend(Guid uid)
        {           
            bool requestSuccess = _friendService.SendFriendRequest(uid, User.Identity.Name);
            return requestSuccess;
        }

        public ActionResult RespondToFriendRequest(FriendRequest request)
        {
            var response = _friendService.RespondToFriendRequest(request.Response, request.Id, User.Identity.Name);
            if (response.IsValid)
            {
                ViewBag.message = response.ValidationMessage;
            }
            else
            {
                ViewBag.message = response.ValidationMessage;
            }
            return PartialView("NotificationsContainer");
        }

        public ActionResult AcceptRequest(FriendRequest request)
        {
            request.Response = RequestResponse.Accepted;
            return RespondToFriendRequest(request);
        }

        public ActionResult DenyRequest(FriendRequest request)
        {
            request.Response = RequestResponse.Denied;
            return RespondToFriendRequest(request);
        }

    }
}