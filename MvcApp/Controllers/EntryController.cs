﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Helpers;
using Domain.Models;
using Domain.Services.EntryServices;
using MvcApp.ViewModels.AssociationViewModels;
using MvcApp.ViewModels.EntryViewModels;

namespace MvcApp.Controllers
{
    public class EntryController : Controller
    {
        private EntryService _service;

        public EntryController(EntryService service)
        {
            _service = service;
        }
        public ActionResult AddFriendEntry(Guid uid)
        {
            UserProfileEntryViewModel model = new UserProfileEntryViewModel();
            model.recieverId = uid;
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult AddFriendEntry(UserProfileEntryViewModel model)
        {
            var result = _service.AddUserEntry(User.Identity.Name, model.recieverId, model.Message, model.IsPrivate);
            if (result.IsValid)
            {
                return RedirectToAction("GetProfileEntries", new { uid = model.recieverId });
            }
            else
            {
                ModelState.AddModelError("Error", result.ValidationMessage);
                return PartialView(model);
            }
        }

        public ActionResult GetProfileEntries(Guid uid)
        {
            List<UserProfileEntryViewModel> model = new List<UserProfileEntryViewModel>();
            var entries = _service.GetUserEntries(uid, User.Identity.Name, 0);
            if (entries != null)
            {
                foreach (var entry in entries)
                {
                    model.Add(new UserProfileEntryViewModel
                    {
                        DateTime = entry.PublishDate,
                        Author = entry.Author.ProfileInfo.Firstname + " " + entry.Author.ProfileInfo.Lastname,
                        Message = entry.Message,
                        IsPrivate = entry.IsPrivate
                    });
                }
            }
            return PartialView(model);
        }

        public ActionResult AddAssocEntry(string id)
        {
            AssociationEntryViewModel model = new AssociationEntryViewModel();
            model.AssocId = id;
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult AddAssocEntry(AssociationEntryViewModel model)
        {
            var result = _service.AddAssociationEntry(User.Identity.Name,int.Parse(Encryption.decrypt(model.AssocId)), model.Message,
                model.AssociationRole, model.IsPrivate);
            return RedirectToAction("GetAssocEntries", new {id = model.AssocId, role = model.AssociationRole } );
        }
        public ActionResult GetAssocEntries(string id, AssociationRole role)
        {
            List<AssociationEntryViewModel> model = new List<AssociationEntryViewModel>();
            var result = _service.GetAssociationEntries(int.Parse(Encryption.decrypt(id)), role, 0);
            if (result != null)
            {
                foreach (var entry in result)
                {
                    model.Add(new AssociationEntryViewModel
                    {
                        AssociationRole = role,
                        Author = entry.Author?.ProfileInfo ?? null,
                        Message = entry.Message,
                        IsPrivate = entry.IsPrivate,
                        PublishDate = entry.PublishDate,
                    });
                }
            }
            return PartialView(model);
        }

        public ActionResult AddEventEntry(string id)
        {
            EventEntryViewModel model = new EventEntryViewModel();
            model.EventId = id;
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult AddEventEntry(EventEntryViewModel model)
        {
            var result = _service.AddEventEntry(User.Identity.Name, int.Parse(Encryption.decrypt(model.EventId)), model.Message,
                model.Role, model.IsPrivate);
            return RedirectToAction("GetEventEntries", new { id = model.EventId, role = model.Role });
        }

        public ActionResult GetEventEntries(string id, EventRole role)
        {
            List<EventEntryViewModel> model = new List<EventEntryViewModel>();
            var result = _service.GetEventEntries(int.Parse(Encryption.decrypt(id)), role, 0);
            if (result != null)
            {
                foreach (var entry in result)
                {
                    model.Add(new EventEntryViewModel
                    {
                        Role = role,
                        Author = entry.Author?.ProfileInfo ?? null,
                        Message = entry.Message,
                        IsPrivate = entry.IsPrivate,
                        PublishDate = entry.PublishDate,
                    });
                }
            }
            return PartialView(model);
        }
    }
}