﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApp.Controllers
{
    public class ErrorController : Controller
    {
        
        public ActionResult NotFound()
        {
            return View();
        }

        public ActionResult Unauthorized()
        {
            return PartialView();
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}