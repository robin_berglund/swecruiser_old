﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApp.ViewModels.SettingsViewModels;

namespace MvcApp.Controllers
{
    public class SettingsController : Controller
    {
        // GET: Settings
        public ActionResult AccountSettings()
        {
            return PartialView();
        }

        public ActionResult AssociationSettings(string id)
        {
            AssociationSettingsViewModel model = new AssociationSettingsViewModel();
            model.Id = id;
            return PartialView(model);
        }

        public ActionResult EventSettings(string id)
        {
            EventSettingsViewModel model = new EventSettingsViewModel();
            model.Id = id;
            return PartialView(model);
        }
    }
}