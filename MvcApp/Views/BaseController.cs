﻿using Domain.Services.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApp.Views
{
    public class BaseController : Controller
    {
        // GET: Base
        private IAuthenticationService _service;

        public BaseController(IAuthenticationService service)
        {
            _service = service;
        }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            _service.AuthenticateRequest(filterContext.HttpContext);
            base.OnAuthorization(filterContext);
        }
    }
}