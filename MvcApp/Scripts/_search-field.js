﻿$(document).ready(function () {

    
    $('.js-search-field').keyup(function() {
        $('.js-search-field').each(function (e) {
                if ($(this).is(':focus')) {
                    var targetContainer = $('#' + $(this).data('target'));
                    targetContainer.html('');
                    var code = e.keyCode || e.which;
                    var val = $.trim($(this).val());
                    if (val.length >= 3) {
                        var url = $(this).data('action');
                        GetResults(val, url, targetContainer);
                    } else if (code == 32 || code == 0) {
                        var url = $(this).data('action');
                        if (val.length > 2) {
                            GetResults(val, url, targetContainer);
                        }
                    }
                }
            });
        }
    );
   
        function GetResults(e, url, container)
        {
            $.ajax({
                url: url,
                contentType: 'application/html; charset=utf-8',
                type: 'GET',
                data: { 'queryString': e },
                success: function (result) {
                    container.css('visibility', 'visible');
                    container.html(result);
                },
                error: function (e, xhr) {
                    console.log(e, xhr);
                }
            });
        }
});