﻿$(document).ready(function () {

    $('.js-modal').on('click', function (e) {
        e.preventDefault();
        var url = $(this).attr('data-action');
        console.log(url);
        $.get(url, function (data) {
            $('.sc-modal-container').html(data);
            $('.sc-modal').modal('show');
        });
    });
   
    
    function loadPartials() {
        var containers = $('.js-partial-load');
        containers.each(function (e) {
            var url = $(this).attr('data-action');
            var container = $(this);
            $.get(url, function (data) {
                $(container).html(data);

            });
        });
    }

    loadPartials();
    

    $('.js-request-friend').on('click', function(e) {
        e.preventDefault();
        var uid = $('.js-uid');
        $.ajax({
            url: 'Profile/FriendModule',
            contentType: 'application/html; charset=utf-8',
            type: 'POST',
            data: { 'uid': uid },
            success: function(result) {
                $('.js-partial-container').html('')
                $('.js-partial-container').html(result);
            },
            error: function(e, xhr) {
                console.log(e, xhr);
            }
        });
    });

    $('.js-read-notifications').on('click', function () {

        var indicator = $(this).find('.js-notification-indicator');
        var val = parseInt(indicator.html(), 10);
        var ids = [];
        if (val > 0) {
            $('.js-noIdContainer').each(function() {
                ids.push(parseInt($(this).val(), 10));
                
            });
            $.ajax({
                url: 'Home/MarkNotificationAsRead',
                datatype: "json",
                type: 'GET',
                traditional: true,
                data: { 'ids': ids },
                success: function (result) {
                    if (val >= 5) {
                        val -= 5;
                        indicator.hide();
                    } else {
                        indicator.hide();
                    }
                    
                },
                error: function (e, xhr) {
                    console.log(e, xhr);
                }
            });
        }
        
    });
        
});

