﻿$(document).ready(function () {

    $('.js-module-hide').css('visibility', 'hidden');

    $('.js-toggle-module').on('click', function () {
        var containerId = $(this).data('container');
        $('.js-module-hide').each(function() {
            if ($(this).attr('id') != containerId) {
                $(this).css('visibility', 'hidden');
            }
        });
        var container = $('#'+containerId);
        if (container.css('visibility') == 'hidden') {
            container.css('visibility', 'visible');
        } else {
            container.css('visibility', 'hidden');
        }
    });

    $(document).mousedown(function (e)
    {
        var container = $('.js-clickout-hide');
        container.each(function () {
            if (!$(this).is(e.target) // if the target of the click isn't the container...
            && $(this).has(e.target).length === 0 && $(this).css('visibility') === 'visible') // ... nor a descendant of the container
        {
            $(this).css('visibility', 'hidden');
        }
        });
    });
});