﻿using Domain.Models;
using Domain.Services.PasswordHandler;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DBInitializer : CreateDatabaseIfNotExists<ScContext>
    {
        IPasswordHandler _pwhandler;
        public DBInitializer()
        {
            _pwhandler = new PasswordHandler();
        }
        protected override void Seed(ScContext context)
        {

            SeedPermissionsAndRoles(context);

            
        }

        void SeedPermissionsAndRoles(ScContext context)
        {
            var role1 = new UserRole { Description = "UnconfirmedMember" };
            var role2 = new UserRole { Description = "ConfirmedMember" };
            var role3 = new UserRole { Description = "Administrator" };

            var permission1 = new Permission { Description = "Administrate" };
            var permission2 = new Permission { Description = "AddFriend" };
            var permission3 = new Permission { Description = "ManageAssociation" };
            var permission4 = new Permission { Description = "ManageEvent" };
            var permission5 = new Permission { Description = "MakeEntrys" };

            context.Permissions.Add(permission1);
            context.Permissions.Add(permission2);
            context.Permissions.Add(permission3);
            context.Permissions.Add(permission4);
            context.Permissions.Add(permission5);

            role1.Permissions = new List<Permission> { permission2, permission5 };
            role2.Permissions = new List<Permission> { permission2, permission3, permission4, permission5 };
            role3.Permissions = new List<Permission> { permission1, permission2, permission3, permission4, permission5 };

            context.UserRoles.Add(role1);
            context.UserRoles.Add(role2);
            context.UserRoles.Add(role3);

            List<ProfileInfo> profileList = new List<ProfileInfo> {
                 new ProfileInfo { City = "Umeå", Firstname = "Lars", Lastname = "Eriksson", FavouriteVehicles = "Dodge Challenger -71" },
                 new ProfileInfo { City = "Visby", Firstname = "Anders", Lastname = "Nilsson", FavouriteVehicles = "Plymouth Barracuda 1970" },
                 new ProfileInfo { City = "Uppsala", Firstname = "Lennart", Lastname = "Lööv", FavouriteVehicles = "Ford Fairline -56" },
                 new ProfileInfo { City = "Stockholm", Firstname = "Hasse", Lastname = "Wahlman", FavouriteVehicles = "Buick LeSabre" },
                 new ProfileInfo { City = "Karlstad", Firstname = "Kent", Lastname = "Moberg", FavouriteVehicles = "Cadillac -1959" },
                 new ProfileInfo { City = "Halmstad", Firstname = "Niklas", Lastname = "Lundberg", FavouriteVehicles = "Chrysler Desoto 1955" },
                 new ProfileInfo { City = "Göreborg", Firstname = "Glenn", Lastname = "Ros", FavouriteVehicles = "Buick Limited -58" },
                 new ProfileInfo { City = "Stockholm", Firstname = "Björn", Lastname = "Backman", FavouriteVehicles = "Ford Roadster -1931" },
                 new ProfileInfo { City = "Norrköping", Firstname = "Lasse", Lastname = "Danielsson", FavouriteVehicles = "Chevrolet Impala 1965" },
                 new ProfileInfo { City = "Mora", Firstname = "Nisse", Lastname = "Forsberg", FavouriteVehicles = "Volvo 142" }
            };


            List<User> userList = new List<User>
            {
                new User { Email = "mail1@example.com",PasswordHash = _pwhandler.HashPassword("test"), ProfileInfo = profileList[0], Role = role1 },
                new User { Email = "mail2@example.com",PasswordHash = _pwhandler.HashPassword("test"), ProfileInfo = profileList[1], Role = role1 },
                new User { Email = "mail3@example.com",PasswordHash = _pwhandler.HashPassword("test"), ProfileInfo = profileList[2], Role = role1 },
                new User { Email = "mail4@example.com",PasswordHash = _pwhandler.HashPassword("test"), ProfileInfo = profileList[3], Role = role1 },
                new User { Email = "mail5@example.com",PasswordHash = _pwhandler.HashPassword("test"), ProfileInfo = profileList[4], Role = role1 },
                new User { Email = "mail6@example.com",PasswordHash = _pwhandler.HashPassword("test"), ProfileInfo = profileList[5], Role = role1 },
                new User { Email = "mail7@example.com",PasswordHash = _pwhandler.HashPassword("test"), ProfileInfo = profileList[6], Role = role1 },
                new User { Email = "mail8@example.com",PasswordHash = _pwhandler.HashPassword("test"), ProfileInfo = profileList[7], Role = role1 },
                new User { Email = "mail9@example.com",PasswordHash = _pwhandler.HashPassword("test"), ProfileInfo = profileList[8], Role = role1 },
                new User { Email = "mail10@example.com",PasswordHash = _pwhandler.HashPassword("test"), ProfileInfo = profileList[9], Role = role1 }
            };

            foreach (var user in userList)
            {
                context.Users.Add(user);
                user.ProfileInfo.ProfileUrl = user.UserId.ToString();
            }

            context.SaveChanges();

            foreach (var user in context.Users)
            {
                user.ProfileInfo.ProfileUrl = user.ProfileInfo.UserId.ToString();
                context.ChangeState(user, EntityState.Modified);
            }
            context.SaveChanges();
        }

        void SeedUsers(ScContext context)
        {
            

        }
    }
       

  
}
