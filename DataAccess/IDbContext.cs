﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IDbContext
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;

        void ChangeState<T>(T entity, EntityState state) where T : class;

        int SaveChanges();

        void Dispose();
    }
}
