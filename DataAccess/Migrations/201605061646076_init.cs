namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AssocEventInvites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Rsvp = c.Int(nullable: false),
                        Event_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Events", t => t.Event_Id)
                .Index(t => t.Event_Id);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Name = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        Category = c.Int(nullable: false),
                        BannerImage_Id = c.Int(),
                        User_UserId = c.Guid(),
                        Association_AssocId = c.Int(),
                        OwnerAssociation_AssocId = c.Int(),
                        OwnerUser_UserId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Images", t => t.BannerImage_Id)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .ForeignKey("dbo.Associations", t => t.Association_AssocId)
                .ForeignKey("dbo.Associations", t => t.OwnerAssociation_AssocId)
                .ForeignKey("dbo.Users", t => t.OwnerUser_UserId)
                .Index(t => t.BannerImage_Id)
                .Index(t => t.User_UserId)
                .Index(t => t.Association_AssocId)
                .Index(t => t.OwnerAssociation_AssocId)
                .Index(t => t.OwnerUser_UserId);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Path = c.String(),
                        VehicleBio_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VehicleBios", t => t.VehicleBio_Id)
                .Index(t => t.VehicleBio_Id);
            
            CreateTable(
                "dbo.Associations",
                c => new
                    {
                        AssocId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Vat = c.Int(nullable: false),
                        PrivacyType = c.Int(nullable: false),
                        Category = c.Int(nullable: false),
                        AssociationOwner_UserId = c.Guid(),
                        BannerImage_Id = c.Int(),
                        Event_Id = c.Int(),
                    })
                .PrimaryKey(t => t.AssocId)
                .ForeignKey("dbo.Users", t => t.AssociationOwner_UserId)
                .ForeignKey("dbo.Images", t => t.BannerImage_Id)
                .ForeignKey("dbo.Events", t => t.Event_Id)
                .Index(t => t.AssociationOwner_UserId)
                .Index(t => t.BannerImage_Id)
                .Index(t => t.Event_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Guid(nullable: false, identity: true),
                        Email = c.String(),
                        IsConfirmedAccount = c.Boolean(nullable: false),
                        AccountConfirmationToken = c.Guid(nullable: false),
                        PasswordHash = c.String(),
                        IsPermanentlyBanned = c.Boolean(nullable: false),
                        SecurityStamp = c.Guid(nullable: false),
                        PasswordRecoveryToken = c.Guid(nullable: false),
                        PasswordRecoveryDeadline = c.DateTime(),
                        User_UserId = c.Guid(),
                        Role_Id = c.Int(),
                        Event_Id = c.Int(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .ForeignKey("dbo.UserRoles", t => t.Role_Id)
                .ForeignKey("dbo.Events", t => t.Event_Id)
                .Index(t => t.User_UserId)
                .Index(t => t.Role_Id)
                .Index(t => t.Event_Id);
            
            CreateTable(
                "dbo.EventInvites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Rsvp = c.Int(nullable: false),
                        IsInvitedByUser = c.Boolean(nullable: false),
                        Event_Id = c.Int(),
                        InvitedByAssoc_AssocId = c.Int(),
                        InvitedByUser_UserId = c.Guid(),
                        InvitedUser_UserId = c.Guid(),
                        User_UserId = c.Guid(),
                        AssocEventInvite_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Events", t => t.Event_Id)
                .ForeignKey("dbo.Associations", t => t.InvitedByAssoc_AssocId)
                .ForeignKey("dbo.Users", t => t.InvitedByUser_UserId)
                .ForeignKey("dbo.Users", t => t.InvitedUser_UserId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .ForeignKey("dbo.AssocEventInvites", t => t.AssocEventInvite_Id)
                .Index(t => t.Event_Id)
                .Index(t => t.InvitedByAssoc_AssocId)
                .Index(t => t.InvitedByUser_UserId)
                .Index(t => t.InvitedUser_UserId)
                .Index(t => t.User_UserId)
                .Index(t => t.AssocEventInvite_Id);
            
            CreateTable(
                "dbo.FriendRequests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        Response = c.Int(nullable: false),
                        IsRead = c.Boolean(nullable: false),
                        RequesterLink = c.String(),
                        Requester_UserId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Requester_UserId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.Requester_UserId);
            
            CreateTable(
                "dbo.FriendShips",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        FriendId = c.Guid(nullable: false),
                        Since = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.FriendId })
                .ForeignKey("dbo.ProfileInfoes", t => t.FriendId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.FriendId);
            
            CreateTable(
                "dbo.ProfileInfoes",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        Firstname = c.String(),
                        Lastname = c.String(),
                        City = c.String(),
                        FavouriteVehicles = c.String(),
                        ProfileUrl = c.String(),
                        ProfileImage_Id = c.Int(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Images", t => t.ProfileImage_Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ProfileImage_Id);
            
            CreateTable(
                "dbo.VehicleBios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Model = c.String(),
                        Year = c.Int(nullable: false),
                        Description = c.String(),
                        ProfileInfo_UserId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProfileInfoes", t => t.ProfileInfo_UserId)
                .Index(t => t.ProfileInfo_UserId);
            
            CreateTable(
                "dbo.AssociationMemberships",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        AssocId = c.Int(nullable: false),
                        Joined = c.DateTime(nullable: false),
                        Role = c.Int(nullable: false),
                        InviteResponse = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.AssocId })
                .ForeignKey("dbo.Associations", t => t.AssocId, cascadeDelete: true)
                .ForeignKey("dbo.ProfileInfoes", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.AssocId);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsRead = c.Boolean(nullable: false),
                        Message = c.String(),
                        User_UserId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.Entries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PublishDate = c.DateTime(nullable: false),
                        Message = c.String(),
                        Author_UserId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Author_UserId)
                .Index(t => t.Author_UserId);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReportDate = c.DateTime(nullable: false),
                        ReasonOfReport = c.Int(nullable: false),
                        AdditionalInfo = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        ReportedByUser_UserId = c.Guid(),
                        ReportedEntry_Id = c.Int(),
                        ReportedImage_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ReportedByUser_UserId)
                .ForeignKey("dbo.Entries", t => t.ReportedEntry_Id)
                .ForeignKey("dbo.Images", t => t.ReportedImage_Id)
                .Index(t => t.ReportedByUser_UserId)
                .Index(t => t.ReportedEntry_Id)
                .Index(t => t.ReportedImage_Id);
            
            CreateTable(
                "dbo.PermissionUserRoles",
                c => new
                    {
                        Permission_Id = c.Int(nullable: false),
                        UserRole_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Permission_Id, t.UserRole_Id })
                .ForeignKey("dbo.Permissions", t => t.Permission_Id, cascadeDelete: true)
                .ForeignKey("dbo.UserRoles", t => t.UserRole_Id, cascadeDelete: true)
                .Index(t => t.Permission_Id)
                .Index(t => t.UserRole_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reports", "ReportedImage_Id", "dbo.Images");
            DropForeignKey("dbo.Reports", "ReportedEntry_Id", "dbo.Entries");
            DropForeignKey("dbo.Reports", "ReportedByUser_UserId", "dbo.Users");
            DropForeignKey("dbo.EventInvites", "AssocEventInvite_Id", "dbo.AssocEventInvites");
            DropForeignKey("dbo.AssocEventInvites", "Event_Id", "dbo.Events");
            DropForeignKey("dbo.Events", "OwnerUser_UserId", "dbo.Users");
            DropForeignKey("dbo.Events", "OwnerAssociation_AssocId", "dbo.Associations");
            DropForeignKey("dbo.Users", "Event_Id", "dbo.Events");
            DropForeignKey("dbo.Associations", "Event_Id", "dbo.Events");
            DropForeignKey("dbo.Events", "Association_AssocId", "dbo.Associations");
            DropForeignKey("dbo.Associations", "BannerImage_Id", "dbo.Images");
            DropForeignKey("dbo.Associations", "AssociationOwner_UserId", "dbo.Users");
            DropForeignKey("dbo.Users", "Role_Id", "dbo.UserRoles");
            DropForeignKey("dbo.PermissionUserRoles", "UserRole_Id", "dbo.UserRoles");
            DropForeignKey("dbo.PermissionUserRoles", "Permission_Id", "dbo.Permissions");
            DropForeignKey("dbo.Entries", "Author_UserId", "dbo.Users");
            DropForeignKey("dbo.Notifications", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.AssociationMemberships", "UserId", "dbo.Users");
            DropForeignKey("dbo.AssociationMemberships", "UserId", "dbo.ProfileInfoes");
            DropForeignKey("dbo.AssociationMemberships", "AssocId", "dbo.Associations");
            DropForeignKey("dbo.Events", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.FriendShips", "UserId", "dbo.Users");
            DropForeignKey("dbo.FriendShips", "FriendId", "dbo.ProfileInfoes");
            DropForeignKey("dbo.ProfileInfoes", "UserId", "dbo.Users");
            DropForeignKey("dbo.ProfileInfoes", "ProfileImage_Id", "dbo.Images");
            DropForeignKey("dbo.VehicleBios", "ProfileInfo_UserId", "dbo.ProfileInfoes");
            DropForeignKey("dbo.Images", "VehicleBio_Id", "dbo.VehicleBios");
            DropForeignKey("dbo.FriendRequests", "UserId", "dbo.Users");
            DropForeignKey("dbo.FriendRequests", "Requester_UserId", "dbo.Users");
            DropForeignKey("dbo.EventInvites", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.EventInvites", "InvitedUser_UserId", "dbo.Users");
            DropForeignKey("dbo.EventInvites", "InvitedByUser_UserId", "dbo.Users");
            DropForeignKey("dbo.EventInvites", "InvitedByAssoc_AssocId", "dbo.Associations");
            DropForeignKey("dbo.EventInvites", "Event_Id", "dbo.Events");
            DropForeignKey("dbo.Users", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.Events", "BannerImage_Id", "dbo.Images");
            DropIndex("dbo.PermissionUserRoles", new[] { "UserRole_Id" });
            DropIndex("dbo.PermissionUserRoles", new[] { "Permission_Id" });
            DropIndex("dbo.Reports", new[] { "ReportedImage_Id" });
            DropIndex("dbo.Reports", new[] { "ReportedEntry_Id" });
            DropIndex("dbo.Reports", new[] { "ReportedByUser_UserId" });
            DropIndex("dbo.Entries", new[] { "Author_UserId" });
            DropIndex("dbo.Notifications", new[] { "User_UserId" });
            DropIndex("dbo.AssociationMemberships", new[] { "AssocId" });
            DropIndex("dbo.AssociationMemberships", new[] { "UserId" });
            DropIndex("dbo.VehicleBios", new[] { "ProfileInfo_UserId" });
            DropIndex("dbo.ProfileInfoes", new[] { "ProfileImage_Id" });
            DropIndex("dbo.ProfileInfoes", new[] { "UserId" });
            DropIndex("dbo.FriendShips", new[] { "FriendId" });
            DropIndex("dbo.FriendShips", new[] { "UserId" });
            DropIndex("dbo.FriendRequests", new[] { "Requester_UserId" });
            DropIndex("dbo.FriendRequests", new[] { "UserId" });
            DropIndex("dbo.EventInvites", new[] { "AssocEventInvite_Id" });
            DropIndex("dbo.EventInvites", new[] { "User_UserId" });
            DropIndex("dbo.EventInvites", new[] { "InvitedUser_UserId" });
            DropIndex("dbo.EventInvites", new[] { "InvitedByUser_UserId" });
            DropIndex("dbo.EventInvites", new[] { "InvitedByAssoc_AssocId" });
            DropIndex("dbo.EventInvites", new[] { "Event_Id" });
            DropIndex("dbo.Users", new[] { "Event_Id" });
            DropIndex("dbo.Users", new[] { "Role_Id" });
            DropIndex("dbo.Users", new[] { "User_UserId" });
            DropIndex("dbo.Associations", new[] { "Event_Id" });
            DropIndex("dbo.Associations", new[] { "BannerImage_Id" });
            DropIndex("dbo.Associations", new[] { "AssociationOwner_UserId" });
            DropIndex("dbo.Images", new[] { "VehicleBio_Id" });
            DropIndex("dbo.Events", new[] { "OwnerUser_UserId" });
            DropIndex("dbo.Events", new[] { "OwnerAssociation_AssocId" });
            DropIndex("dbo.Events", new[] { "Association_AssocId" });
            DropIndex("dbo.Events", new[] { "User_UserId" });
            DropIndex("dbo.Events", new[] { "BannerImage_Id" });
            DropIndex("dbo.AssocEventInvites", new[] { "Event_Id" });
            DropTable("dbo.PermissionUserRoles");
            DropTable("dbo.Reports");
            DropTable("dbo.Permissions");
            DropTable("dbo.UserRoles");
            DropTable("dbo.Entries");
            DropTable("dbo.Notifications");
            DropTable("dbo.AssociationMemberships");
            DropTable("dbo.VehicleBios");
            DropTable("dbo.ProfileInfoes");
            DropTable("dbo.FriendShips");
            DropTable("dbo.FriendRequests");
            DropTable("dbo.EventInvites");
            DropTable("dbo.Users");
            DropTable("dbo.Associations");
            DropTable("dbo.Images");
            DropTable("dbo.Events");
            DropTable("dbo.AssocEventInvites");
        }
    }
}
