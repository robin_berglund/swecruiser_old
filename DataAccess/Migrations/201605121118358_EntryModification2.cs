namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntryModification2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Entries", "IsPrivate", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Entries", "IsPrivate");
        }
    }
}
