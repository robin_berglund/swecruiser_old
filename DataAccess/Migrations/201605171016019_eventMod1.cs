namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class eventMod1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventParticipations", "Role", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventParticipations", "Role");
        }
    }
}
