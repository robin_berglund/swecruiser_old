namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntryModification : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Entries", "Author_UserId", "dbo.Users");
            AddColumn("dbo.Entries", "AuthorId", c => c.Guid(nullable: false));
            AddColumn("dbo.Entries", "UserId", c => c.Guid());
            AddColumn("dbo.Entries", "AssocId", c => c.Int());
            AddColumn("dbo.Entries", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Entries", "UserId");
            CreateIndex("dbo.Entries", "AssocId");
            AddForeignKey("dbo.Entries", "AssocId", "dbo.Associations", "AssocId");
            AddForeignKey("dbo.Entries", "UserId", "dbo.Users", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Entries", "UserId", "dbo.Users");
            DropForeignKey("dbo.Entries", "AssocId", "dbo.Associations");
            DropIndex("dbo.Entries", new[] { "AssocId" });
            DropIndex("dbo.Entries", new[] { "UserId" });
            DropColumn("dbo.Entries", "Discriminator");
            DropColumn("dbo.Entries", "AssocId");
            DropColumn("dbo.Entries", "UserId");
            DropColumn("dbo.Entries", "AuthorId");
            AddForeignKey("dbo.Entries", "Author_UserId", "dbo.Users", "UserId");
        }
    }
}
