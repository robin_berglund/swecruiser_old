namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationMod : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "NotoficationType", c => c.Int(nullable: false));
            AddColumn("dbo.Notifications", "LinkId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notifications", "LinkId");
            DropColumn("dbo.Notifications", "NotoficationType");
        }
    }
}
