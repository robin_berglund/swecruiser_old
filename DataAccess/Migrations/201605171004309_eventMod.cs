namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class eventMod : DbMigration
    {
        public override void Up()
        {
            
            DropForeignKey("dbo.EventInvites", "InvitedByUser_UserId", "dbo.Users");
            DropForeignKey("dbo.EventInvites", "InvitedUser_UserId", "dbo.Users");
            DropForeignKey("dbo.Events", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.Users", "Event_Id", "dbo.Events");
            DropForeignKey("dbo.Events", "OwnerUser_UserId", "dbo.Users");
            DropForeignKey("dbo.EventInvites", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.EventInvites", "Event_Id", "dbo.Events");
            RenameTable(name: "dbo.EventInvites", newName: "EventParticipations");
            DropIndex("dbo.Events", new[] { "User_UserId" });
            DropIndex("dbo.Events", new[] { "OwnerUser_UserId" });
            DropIndex("dbo.Users", new[] { "Event_Id" });
            DropIndex("dbo.EventParticipations", new[] { "Event_Id" });
            DropIndex("dbo.EventParticipations", new[] { "InvitedByUser_UserId" });
            DropIndex("dbo.EventParticipations", new[] { "InvitedUser_UserId" });
            DropIndex("dbo.EventParticipations", new[] { "User_UserId" });
            RenameColumn(table: "dbo.EventParticipations", name: "User_UserId", newName: "UserId");
            RenameColumn(table: "dbo.EventParticipations", name: "Event_Id", newName: "EventId");
            DropPrimaryKey("dbo.EventParticipations");
            AddColumn("dbo.Entries", "EventId", c => c.Int());
            AddColumn("dbo.EventParticipations", "InviterUserId", c => c.Guid(nullable: false));
            AlterColumn("dbo.EventParticipations", "EventId", c => c.Int(nullable: false));
            AlterColumn("dbo.EventParticipations", "UserId", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.EventParticipations", new[] { "UserId", "EventId" });
            CreateIndex("dbo.EventParticipations", "UserId");
            CreateIndex("dbo.EventParticipations", "EventId");
            CreateIndex("dbo.Entries", "EventId");
            AddForeignKey("dbo.EventParticipations", "UserId", "dbo.ProfileInfoes", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.Entries", "EventId", "dbo.Events", "Id");
            AddForeignKey("dbo.EventParticipations", "UserId", "dbo.Users", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.EventParticipations", "EventId", "dbo.Events", "Id", cascadeDelete: true);
            DropColumn("dbo.Events", "User_UserId");
            DropColumn("dbo.Events", "OwnerUser_UserId");
            DropColumn("dbo.Users", "Event_Id");
            DropColumn("dbo.EventParticipations", "Id");
            DropColumn("dbo.EventParticipations", "InvitedByUser_UserId");
            DropColumn("dbo.EventParticipations", "InvitedUser_UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EventParticipations", "InvitedUser_UserId", c => c.Guid());
            AddColumn("dbo.EventParticipations", "InvitedByUser_UserId", c => c.Guid());
            AddColumn("dbo.EventParticipations", "Id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.Users", "Event_Id", c => c.Int());
            AddColumn("dbo.Events", "OwnerUser_UserId", c => c.Guid());
            AddColumn("dbo.Events", "User_UserId", c => c.Guid());
            DropForeignKey("dbo.EventParticipations", "EventId", "dbo.Events");
            DropForeignKey("dbo.EventParticipations", "UserId", "dbo.Users");
            DropForeignKey("dbo.Entries", "EventId", "dbo.Events");
            DropForeignKey("dbo.EventParticipations", "UserId", "dbo.ProfileInfoes");
            DropIndex("dbo.Entries", new[] { "EventId" });
            DropIndex("dbo.EventParticipations", new[] { "EventId" });
            DropIndex("dbo.EventParticipations", new[] { "UserId" });
            DropPrimaryKey("dbo.EventParticipations");
            AlterColumn("dbo.EventParticipations", "UserId", c => c.Guid());
            AlterColumn("dbo.EventParticipations", "EventId", c => c.Int());
            DropColumn("dbo.EventParticipations", "InviterUserId");
            DropColumn("dbo.Entries", "EventId");
            AddPrimaryKey("dbo.EventParticipations", "Id");
            RenameColumn(table: "dbo.EventParticipations", name: "EventId", newName: "Event_Id");
            RenameColumn(table: "dbo.EventParticipations", name: "UserId", newName: "User_UserId");
            CreateIndex("dbo.EventParticipations", "User_UserId");
            CreateIndex("dbo.EventParticipations", "InvitedUser_UserId");
            CreateIndex("dbo.EventParticipations", "InvitedByUser_UserId");
            CreateIndex("dbo.EventParticipations", "Event_Id");
            CreateIndex("dbo.Users", "Event_Id");
            CreateIndex("dbo.Events", "OwnerUser_UserId");
            CreateIndex("dbo.Events", "User_UserId");
            AddForeignKey("dbo.EventInvites", "Event_Id", "dbo.Events", "Id");
            AddForeignKey("dbo.EventInvites", "User_UserId", "dbo.Users", "UserId");
            AddForeignKey("dbo.Events", "OwnerUser_UserId", "dbo.Users", "UserId");
            AddForeignKey("dbo.Users", "Event_Id", "dbo.Events", "Id");
            AddForeignKey("dbo.Events", "User_UserId", "dbo.Users", "UserId");
            AddForeignKey("dbo.EventInvites", "InvitedUser_UserId", "dbo.Users", "UserId");
            AddForeignKey("dbo.EventInvites", "InvitedByUser_UserId", "dbo.Users", "UserId");
            RenameTable(name: "dbo.EventParticipations", newName: "EventInvites");
        }
    }
}
