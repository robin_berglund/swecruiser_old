namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationMod2 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Notifications", new[] { "User_UserId" });
            RenameColumn(table: "dbo.Notifications", name: "User_UserId", newName: "UserId");
            AlterColumn("dbo.Notifications", "UserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Notifications", "UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Notifications", new[] { "UserId" });
            AlterColumn("dbo.Notifications", "UserId", c => c.Guid());
            RenameColumn(table: "dbo.Notifications", name: "UserId", newName: "User_UserId");
            CreateIndex("dbo.Notifications", "User_UserId");
        }
    }
}
