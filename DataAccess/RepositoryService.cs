﻿using Domain.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class RepositoryService<TEntity> : IRepositoryService<TEntity> where TEntity : class
    {
        private IDbContext dbContext;

        public RepositoryService(IDbContext context)
        {
            this.dbContext = context;

        }


        private IDbSet<TEntity> Entities
        {
            get { return this.dbContext.Set<TEntity>(); }
        }


        public IQueryable<TEntity> GetAll()
        {
            return Entities.AsQueryable();
        }

        public TEntity GetById(object id)
        {
            return Entities.Find(id);
        }

        public TEntity Insert(TEntity entity)
        {
            try
            {
                var property = entity.GetType().GetProperty("StatusCode");
                if (property != null)
                {
                    var val = property.GetValue(entity, null);
                    if (val.ToString().Equals("0"))
                        property.SetValue(entity, 1000, null);
                }


                Entities.Add(entity);
                this.dbContext.SaveChanges();
                return entity;
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }
        public IQueryable<TEntity> InsertMany(ICollection<TEntity> entities)
        {
            try
            {
                foreach (var entity in entities)
                {
                    var property = entity.GetType().GetProperty("StatusCode");
                    if (property != null)
                    {
                        var val = property.GetValue(entity, null);
                        if (val.ToString().Equals("0"))
                            property.SetValue(entity, 1000, null);
                    }


                    Entities.Add(entity);
                }
                
                this.dbContext.SaveChanges();
                return entities.AsQueryable();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public TEntity Update(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");
            this.dbContext.ChangeState(entity, System.Data.Entity.EntityState.Modified);
            this.dbContext.SaveChanges();
            return entity;
        }
   

        public IQueryable<TEntity> UpdateMany(ICollection<TEntity> entities)
        {
            if (entities == null)
                throw new ArgumentNullException("entity");
            foreach (var entity in entities)
            {
                this.dbContext.ChangeState(entity, System.Data.Entity.EntityState.Modified);
            }
            
            this.dbContext.SaveChanges();
            return entities.AsQueryable();
        }

        public void Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");
            this.dbContext.ChangeState(entity, System.Data.Entity.EntityState.Deleted);
            this.dbContext.SaveChanges();
        }



        public TEntity FindSingle(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate = null, params System.Linq.Expressions.Expression<Func<TEntity, object>>[] includes)
        {
            var set = FindIncluding(includes);
            return (predicate == null) ?
                   set.FirstOrDefault() :
                   set.FirstOrDefault(predicate);
        }

        public TEntity FindSingleIncluding(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate = null, params System.Linq.Expressions.Expression<Func<TEntity, object>>[] includes)
        {
            var set = FindIncluding(includes);
            return (predicate == null) ?
                   set.FirstOrDefault() :
                   set.FirstOrDefault(predicate);
        }

        public IQueryable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate = null, params System.Linq.Expressions.Expression<Func<TEntity, object>>[] includes)
        {
            var set = FindIncluding(includes);
            return (predicate == null) ? set : set.Where(predicate);
        }

        public IQueryable<TEntity> FindIncluding(params System.Linq.Expressions.Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = null;
            if (includeProperties != null)
            {
                query = Entities.AsQueryable();
                foreach (var include in includeProperties)
                {
                    query = query.Include(include);
                }
            }
            return query ?? Entities.AsQueryable();
        }

        public IQueryable<TEntity> Include(params System.Linq.Expressions.Expression<Func<TEntity, object>>[] includes)
        {

            IQueryable<TEntity> entityQuery = dbContext.Set<TEntity>();
            if (includes != null)
            {
                foreach (var include in includes)
                {
                    entityQuery = entityQuery.Include(include);

                }

            }

            return entityQuery.AsQueryable();
        }

        public int Count(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate = null)
        {           
            return (predicate == null) ?
                   Entities.Count() :
                   Entities.Count(predicate);
        }

        public bool Exist(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate = null)
        {
            return (predicate == null) ? Entities.Any() : Entities.Any(predicate);
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.dbContext != null)
                {
                    this.dbContext.Dispose();
                    this.dbContext = null;
                }
            }
        }

      
    }
}
