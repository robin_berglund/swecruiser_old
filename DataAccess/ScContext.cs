﻿using Domain.Models;
using Domain.Models.BaseModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ScContext : DbContext, IDbContext
    {

        public ScContext()
          : base("ScContext")
        {
            Database.SetInitializer(new DBInitializer());
        }
        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public void ChangeState<T>(T entity, EntityState state) where T : class
        {
            Entry<T>(entity).State = state;
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Event> Events { get; set; }

        public DbSet<Association> Associations { get; set; }

        public DbSet<AssociationMembership> AssociationMemberships { get; set; }

        public DbSet<Entry> Entrys { get; set; }

        public DbSet<EventParticipation> EventInvites { get; set; }

        public DbSet<AssocEventInvite> AssocEventInvites { get; set; }

        public DbSet<Report> Reports { get; set; }

        public DbSet<Notification> Notifications { get; set; }

        public DbSet<FriendRequest> FriendRequests { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<Permission> Permissions { get; set; }

        public DbSet<Image> Images { get; set; }

        public DbSet<VehicleBio> VehicleBios { get; set; }

        public DbSet<ProfileInfo> ProfileInfos { get; set; }

        public DbSet<FriendShip> FriendShips { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<FriendShip>().HasKey(f => new { f.UserId, f.FriendId });
            //    modelBuilder.Entity<FriendShip>()
            //      .HasRequired(f => f.User)
            //      .WithMany()
            //      .HasForeignKey(f => f.UserId);
            //    modelBuilder.Entity<FriendShip>()
            //        .HasRequired(f => f.Friend)
            //        .WithMany()
            //        .HasForeignKey(f => f.FriendId)
            //        .WillCascadeOnDelete(false);


            OnUserModelCreating(modelBuilder);
            OnFriendShipModelCreating(modelBuilder);
            OnProfileInfoModelCreating(modelBuilder);  
            OnUserEntryModelCreating(modelBuilder);      
            OnAssociationEntryModelCreating(modelBuilder);   
            OnEventEntryModelCreating(modelBuilder);      

        }

        public void OnProfileInfoModelCreating(DbModelBuilder modelBuilder)
        {
            
        }

        public void OnUserModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<User>().HasMany(u => u.Friends)
            //   .WithRequired(a => a.User).HasForeignKey(a => a.UserId)
            //   .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>().HasMany(u => u.FriendRequests)
               .WithRequired(a => a.User).HasForeignKey(a => a.UserId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>().HasMany(u => u.Notifications)
               .WithRequired(a => a.User).HasForeignKey(a => a.UserId)
               .WillCascadeOnDelete(false);

        }

        public void OnFriendShipModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FriendShip>()
                        .HasKey(a => new { a.UserId, a.FriendId });

            modelBuilder.Entity<FriendShip>().HasRequired(x => x.Friend)
            .WithMany()
            .HasForeignKey(x => x.FriendId)
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<FriendShip>().HasRequired(x => x.User)
                .WithMany(u => u.Friends)
                .HasForeignKey(x => x.UserId)
                .WillCascadeOnDelete(false);
        }

        public void OnUserEntryModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntry>().HasKey(a => new {a.AuthorId, a.UserId});

            modelBuilder.Entity<UserEntry>().HasRequired(x => x.Author)
                .WithMany()
                .HasForeignKey(x => x.AuthorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserEntry>().HasRequired(x => x.User)
                .WithMany(u => u.ProfileEntries)
                .HasForeignKey(x => x.UserId)
                .WillCascadeOnDelete(false);
        }

        public void OnAssociationEntryModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AssociationEntry>().HasKey(a => new { a.AuthorId, a.AssocId });

            modelBuilder.Entity<AssociationEntry>().HasRequired(x => x.Author)
                .WithMany()
                .HasForeignKey(x => x.AuthorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AssociationEntry>().HasRequired(x => x.Association)
                .WithMany(u => u.WallEntries)
                .HasForeignKey(x => x.AssocId)
                .WillCascadeOnDelete(false);
        }

        public void OnEventEntryModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EventEntry>().HasKey(a => new { a.AuthorId, a.EventId });

            modelBuilder.Entity<EventEntry>().HasRequired(x => x.Author)
                .WithMany()
                .HasForeignKey(x => x.AuthorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EventEntry>().HasRequired(x => x.Event)
                .WithMany(u => u.WallEntries)
                .HasForeignKey(x => x.EventId)
                .WillCascadeOnDelete(false);
        }

        public void OnEventModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EventParticipation>()
                        .HasKey(a => new { a.UserId, a.EventId });

            modelBuilder.Entity<EventParticipation>().HasRequired(x => x.InvitedByUser)
            .WithMany()
            .HasForeignKey(x => x.InviterUserId)
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<EventParticipation>().HasRequired(x => x.InvitedUser)
                .WithMany(u => u.User.EventInvites)
                .HasForeignKey(x => x.UserId)
                .WillCascadeOnDelete(false);
        }
    }
}
