﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Helpers;
using Domain.Models;
using Domain.Services.Abstractions;

namespace Domain.Services.UserServices
{
    public class UserService : IUserService 
    {
        IRepositoryService<User> _userRepository;
        public UserService(IRepositoryService<User> userRepository)
        {
            _userRepository = userRepository;
        }
        public bool DeleteUser(string email)
        {
            throw new NotImplementedException();
        }

        public User GetUserByEmail(string email)
        {
            return _userRepository.FindSingle(x => x.Email == email,  x => x.ProfileInfo, c => c.FriendRequests, v => v.Friends);
        }

        public User GetUserById(Guid userId)
        {
            return _userRepository.FindSingle(x => x.UserId == userId, x => x.ProfileInfo, c => c.FriendRequests, v => v.Friends);
        }

        public List<Permission> GetUserPermissions(string email)
        {
            return _userRepository.FindSingle(x => x.Email == email).Role.Permissions.ToList();
        }

        public void UpdateUserRecords(User user)
        {
            try
            {
                _userRepository.Update(user);
            }
            catch (Exception)
            {
                throw;
            } 
        }


        public User GetNotifications(string email)
        {
            try
            {
                var userQuery = _userRepository.Include(x => x.FriendRequests, x => x.Notifications )
                    .Where(x => x.Email == email)
                    .Select( u => new
                    {
                        user = u,
                        requests = u.FriendRequests.OrderBy(x => x.IsRead).Take(5).ToList(),
                        notifications = u.Notifications.OrderBy(x => x.IsRead).Take(5).ToList()
                    })
                    .FirstOrDefault();
                var user = new User();

                user = userQuery.user;
                user.FriendRequests = userQuery.requests;
                user.Notifications = userQuery.notifications;

                return user;
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }

        public bool MarkNotificationsAsRead(int[] ids, string email)
        {
            
            try
            {
                var user = _userRepository.FindSingle(x => x.Email == email, x => x.Notifications);
                for (int i = 0; i < ids.Length; i++)
                {
                    var notification = user.Notifications.FirstOrDefault(x => x.Id == ids[i]);
                    if (notification != null)
                        notification.IsRead = true;
                }
                _userRepository.Update(user);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        public bool MarkFriendRequestsAsRead(int[] ids, string email)
        {
            try
            {
                var user = _userRepository.FindSingle(x => x.Email == email, x => x.FriendRequests);
                for (int i = 0; i < ids.Length; i++)
                {
                    var request = user.FriendRequests.FirstOrDefault(x => x.Id == ids[i]);
                    if (request != null)
                        request.IsRead = true;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UsersAreFriends(string visitorEmail, Guid user)
        {
            var visitor = _userRepository.FindSingle(x => x.Email == visitorEmail, x => x.Friends);
            return visitor.Friends.FirstOrDefault(x => x.FriendId == user) != null ;
        }
    }
}

