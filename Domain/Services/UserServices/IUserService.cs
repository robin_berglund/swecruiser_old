﻿using Domain.Models;
using System;
using System.Collections.Generic;
using Domain.Helpers;

namespace Domain.Services.UserServices
{
    public interface IUserService
    {
        User GetUserById(Guid userId);

        User GetUserByEmail(string email);
        
        List<Permission> GetUserPermissions(string email);

        void UpdateUserRecords(User user);

        bool DeleteUser(string email);

        User GetNotifications(string email);

        bool MarkNotificationsAsRead(int[] ids, string email);

        bool MarkFriendRequestsAsRead(int[] ids, string email);

        bool UsersAreFriends(string visitorEmail, Guid user);



    }
}
