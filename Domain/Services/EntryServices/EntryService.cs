﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Helpers;
using Domain.Models;
using Domain.Services.Abstractions;

namespace Domain.Services.EntryServices
{
    public class EntryService : IEntryService
    {
        private IRepositoryService<User> _userRepository;
        private IRepositoryService<Association> _assocRepository;
        private IRepositoryService<UserEntry> _userEntryRepository;
        private IRepositoryService<AssociationEntry> _assocEntryRepository;
        private IRepositoryService<Notification> _notificationRepository;
        private IRepositoryService<EventEntry> _eventEntryRepository;
        private IRepositoryService<Event> _eventRepository; 



        public EntryService(IRepositoryService<User> userRepository, 
            IRepositoryService<Association> assocRepository, 
            IRepositoryService<UserEntry> userEntryRepository, 
            IRepositoryService<AssociationEntry> assocEntryRepository, 
            IRepositoryService<Notification> notificationRepository, 
            IRepositoryService<EventEntry> eventEntryRepository, 
            IRepositoryService<Event> eventRepository)
        {
            _userRepository = userRepository;
            _assocRepository = assocRepository;
            _userEntryRepository = userEntryRepository;
            _assocEntryRepository = assocEntryRepository;
            _notificationRepository = notificationRepository;
            _eventEntryRepository = eventEntryRepository;
            _eventRepository = eventRepository;
        }



        public ScValidationResult AddUserEntry(string authorEmail, Guid recieverId, string message, bool isPrivate)
        {
            var result = new ScValidationResult();

            try
            {
                var author = _userRepository.FindSingle(x => x.Email == authorEmail, x => x.ProfileInfo);
                var entry = new UserEntry
                {
                    Author = author,
                    Message = message,
                    PublishDate = DateTime.Now,
                    IsPrivate = isPrivate
                };
                var reciever = _userRepository.FindSingle(x => x.UserId == recieverId);
                reciever.ProfileEntries = new List<UserEntry>();
                reciever.ProfileEntries.Add(entry);
                reciever.Notifications = new List<Notification>();
                reciever.Notifications.Add(new Notification
                {
                    Message = author.ProfileInfo.Firstname + " "+ author.ProfileInfo.Lastname + " har skrivit ett meddelande till dig.",
                    NotoficationType = NotoficationType.ProfileEntry
                });
                _userRepository.Update(reciever);
                result.IsValid = true;
                result.ValidationMessage = "meddelande skickat";
            }
            catch (Exception)
            {
                result.ValidationMessage = "Något gick fel när meddelandet skulle sparas";
            }
            return result;
        }

        public ScValidationResult AddAssociationEntry(string authorEmail, int assocId, string message, AssociationRole role, bool isPrivate)
        {
            ScValidationResult result = new ScValidationResult();
            try
            {
                var author = _userRepository.FindSingle(x => x.Email == authorEmail, x => x.ProfileInfo);
                AssociationEntry entry = new AssociationEntry
                {
                    Author = author ?? null,
                    AssocId = assocId,
                    Message = message,
                    IsPrivate = isPrivate,
                    PublishDate = DateTime.Now
                };
                var association = _assocRepository.FindSingle(x => x.AssocId == assocId, x => x.Members);
                association.WallEntries = new List<AssociationEntry>();
                association.WallEntries.Add(entry);
                ICollection<Notification> notifications = new List<Notification>();
                Notification notification = new Notification { Message = author.ProfileInfo.Firstname + " " + author.ProfileInfo.Lastname + " har skrivit ett meddelande i " + association.Name};
                foreach (var member in association.Members)
                {
                    notifications.Add(new Notification { Message = notification.Message, UserId = member.UserId, NotoficationType = NotoficationType.AssociationEntry, LinkId = Encryption.encrypt(association.AssocId.ToString())});
                }
                _notificationRepository.InsertMany(notifications);
                result.IsValid = true;
                result.ValidationMessage = "Meddelande skickat!";
            }
            catch (Exception)
            {
                result.ValidationMessage = "något gick fel när meddelandet skulle sparas.";
            }
            return result;
        }

        public ScValidationResult AddEventEntry(string authorEmail, int eventId, string message, EventRole role, bool isPrivate)
        {
            ScValidationResult result = new ScValidationResult();
            try
            {
                var author = _userRepository.FindSingle(x => x.Email == authorEmail, x => x.ProfileInfo);
                EventEntry entry = new EventEntry
                {
                    Author = author ?? null,
                    Message = message,
                    IsPrivate = isPrivate,
                    PublishDate = DateTime.Now
                };
                var theEvent = _eventRepository.FindSingle(x => x.Id == eventId, x => x.Invites);
                theEvent.WallEntries = new List<EventEntry>();
                theEvent.WallEntries.Add(entry);
                ICollection<Notification> notifications = new List<Notification>();
                Notification notification = new Notification { Message = author.ProfileInfo.Firstname + " " + author.ProfileInfo.Lastname + " har skrivit ett meddelande i " + theEvent.Name };
                foreach (var member in theEvent.Invites.Where(x => x.Rsvp != Rsvp.NotAttending))
                {
                    notifications.Add(new Notification { Message = notification.Message, UserId = member.UserId, NotoficationType = NotoficationType.AssociationEntry, LinkId = Encryption.encrypt(theEvent.Id.ToString()) });
                }
                _notificationRepository.InsertMany(notifications);
                result.IsValid = true;
                result.ValidationMessage = "Meddelande skickat!";
            }
            catch (Exception)
            {
                result.ValidationMessage = "något gick fel när meddelandet skulle sparas.";
            }
            return result;
        }

        public List<UserEntry> GetUserEntries(Guid uid,string visitorEmail, int index)
        {
            try
            {
                var user = _userRepository.FindSingle(x => x.UserId == uid);
                List<UserEntry> entryList = new List<UserEntry>();
                if (user.Email == visitorEmail)
                {
                    entryList = _userEntryRepository.Find(x => x.UserId == user.UserId, x => x.Author.ProfileInfo)
                        .OrderByDescending(x => x.PublishDate).Take(5).ToList();
                }
                else
                {
                    entryList = _userEntryRepository.Find(x => x.UserId == user.UserId && (x.IsPrivate == false || x.Author.Email == visitorEmail), x => x.Author.ProfileInfo)
                        .OrderByDescending(x => x.PublishDate).Take(5)
                        .ToList();
                }
                return entryList;
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        

        public List<AssociationEntry> GetAssociationEntries(int assocId, AssociationRole role, int index)
        {
            try
            {
                List<AssociationEntry> entries = new List<AssociationEntry>();
                if (role == AssociationRole.NoMember)
                {
                    entries = _assocEntryRepository.Find(x => x.AssocId == assocId && x.IsPrivate == false, x => x.Author.ProfileInfo)
                        .OrderByDescending(x => x.PublishDate)
                        .Take(10).ToList();
                }
                else
                {
                    entries =  _assocEntryRepository.Find(x => x.AssocId == assocId, x => x.Author.ProfileInfo)
                        .OrderByDescending(x => x.PublishDate)
                        .Take(10).ToList();
                }
                return entries;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<EventEntry> GetEventEntries(int eventId, EventRole role, int index)
        {
            try
            {
                List<EventEntry> entries = new List<EventEntry>();
                if (role == EventRole.NotInvited || role == EventRole.NotAccepted)
                {
                    entries = _eventEntryRepository.Find(x => x.EventId == eventId && x.IsPrivate == false, x => x.Author.ProfileInfo)
                        .OrderByDescending(x => x.PublishDate)
                        .Take(10).ToList();
                }
                else
                {
                    entries = _eventEntryRepository.Find(x => x.EventId == eventId, x => x.Author.ProfileInfo)
                        .OrderByDescending(x => x.PublishDate)
                        .Take(10).ToList();
                }
                return entries;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
