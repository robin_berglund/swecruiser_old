﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Helpers;
using Domain.Models;

namespace Domain.Services.EntryServices
{
    public interface IEntryService
    {
        ScValidationResult AddUserEntry(string authorEmail, Guid recieverId, string message, bool isPrivate);

        ScValidationResult AddAssociationEntry(string authorEmail, int assocId, string message, AssociationRole role, bool isPrivate);

        ScValidationResult AddEventEntry(string authorEmail, int eventId, string message, EventRole role, bool isPrivate);

        List<UserEntry> GetUserEntries(Guid uid, string visitorEmail, int index);

        List<AssociationEntry> GetAssociationEntries(int assocId,AssociationRole role, int index);

        List<EventEntry> GetEventEntries(int eventId, EventRole role, int index);
    }
}
