﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Domain.Helpers;
using Domain.Models;
using Domain.Services.Abstractions;

namespace Domain.Services.AssociationServices
{
    public class AssociationService : IAssociationService
    {
        private IRepositoryService<Association> _associationRepository;
        private IRepositoryService<AssociationMembership> _associationMembershipRepository;
        private IRepositoryService<ProfileInfo> _profieInfoRepository;
        private IRepositoryService<User> _userRepository;
        private IRepositoryService<Notification> _notificationRepository; 
        public AssociationService(IRepositoryService<Association> associationRepository, 
            IRepositoryService<AssociationMembership> associationMembershipRepository, 
            IRepositoryService<ProfileInfo> profieInfoRepository,
            IRepositoryService<User> userRepository,
            IRepositoryService<Notification> notificationRepository)
        {
            _associationRepository = associationRepository;
            _associationMembershipRepository = associationMembershipRepository;
            _profieInfoRepository = profieInfoRepository;
            _userRepository = userRepository;
            _notificationRepository = notificationRepository;
        }

        public ScValidationResult CreateAssociation(string name, int vat, Image bannerImage, User associationCreator, AssociationType category, AssociationPrivacy privacyType)
        {
            ScValidationResult result = new ScValidationResult();
            try
            {
                if (_associationRepository.FindSingle(x => x.Name == name) != null)
                {
                    result.ValidationMessage = "Det angivna organisationsnamnet är redan upptaget. Prova ett annat!";
                }
                else
                {
                    Association association = new Association
                    {
                        Name = name,
                        Vat = vat != 0 ? vat : 0,
                        BannerImage = bannerImage,
                        Category = category,
                        PrivacyType = privacyType,
                    };
                    association.Members = new List<AssociationMembership>
                    {
                        new AssociationMembership
                        {
                            UserId = associationCreator.UserId,
                            Association = association,
                            InviteResponse = RequestResponse.Accepted,
                            Role = AssociationRole.Owner,
                            Joined = DateTime.Now
                        }
                    };
                    _associationRepository.Insert(association);
                    result.IsValid = true;
                    result.ValidationMessage = "Organisation skapad!";
                }
                
            }
            catch (Exception)
            {
                result.ValidationMessage = "Något gick fel när organisationen skulle skapas.";
            }
            return result;
        }

        public ScValidationResult EditAssociation(Association association)
        {
            ScValidationResult result = new ScValidationResult();
            try
            {
                _associationRepository.Update(association);
                result.IsValid = true;
                result.ValidationMessage = "Organisationsinfo sparad!";
            }
            catch (Exception)
            {
                result.ValidationMessage = "Något gick fel när organisationen skulle uppdateras.";
            }
            return result;
        }

        public Association GetAssociation(string name, int id)
        {
            try
            {
                return _associationRepository.FindSingle(x => x.Name.Equals(name) && x.AssocId == id, c => c.Members);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Association GetAssociationById(int id)
        {
            try
            {
                return _associationRepository.FindSingle(x => x.AssocId == id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ScValidationResult AddMember(AssociationMembership member)
        {
            ScValidationResult result = new ScValidationResult();
            try
            {
                var association =  _associationRepository.FindSingle(x => x.AssocId == member.AssocId, c => c.Members);
                association.Members.Add(member);
                _associationRepository.Update(association);
                result.IsValid = true;
                result.ValidationMessage = "Medlem inbjuden.";
            }
            catch (Exception)
            {
                result.ValidationMessage = "Något gick fel när medlemmen skulle läggas till.";
            }
            return result;
        }

        public List<AssociationMembership> GetInvites(Guid uid)
        {
            try
            {
                return _associationMembershipRepository.Find(x => x.UserId == uid).ToList();
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public ScValidationResult RespondToInvite(AssociationMembership invite, string email, int notificationId)
        {
            ScValidationResult result = new ScValidationResult();
            var response = invite.InviteResponse;
            try
            {
                var user = _userRepository.FindSingle(x => x.Email == email);
                invite =
                    _associationMembershipRepository.FindSingle(
                        x => x.AssocId == invite.AssocId && x.UserId == user.UserId, x => x.Association);
            }
            catch (Exception)
            {
                result.ValidationMessage = "Ett fel uppstod vid svar på inbjudan.";
            }
            if (response == RequestResponse.Accepted)
            {
                try
                {
                    invite.InviteResponse = RequestResponse.Accepted;
                    var notification = _notificationRepository.FindSingle(x => x.Id == notificationId);
                    notification.NotoficationType = NotoficationType.Info;
                    notification.Message = "Du gick med i " + invite.Association.Name;
                    _notificationRepository.Update(notification);
                    _associationMembershipRepository.Update(invite);
                    result.IsValid = true;
                    result.ValidationMessage = "Inbjudan accepterad.";
                }
                catch (Exception)
                {
                    result.ValidationMessage = "Något gick fel vid bekräftelse av inbjudan.";
                }
            }
            else if (response == RequestResponse.Denied)
            {
                try
                {
                    var notification = _notificationRepository.FindSingle(x => x.Id == notificationId);
                    _notificationRepository.Delete(notification);
                    _associationMembershipRepository.Delete(invite);
                    result.IsValid = true;
                    result.ValidationMessage = "Inbjudan borttagen";
                    
                }
                catch (Exception)
                {
                    result.ValidationMessage = "Något gick fel vid nekan av inbjudan";
                }
            }
            
            return result;
        }

        public List<AssociationSearchResult> FindAssociations(string query)
        {
            try
            {
                var associations = _associationRepository.Find(x => x.Name.Contains(query))
                    .Select(x => new AssociationSearchResult
                    {
                        AssociationType =  x.Category,
                        Name = x.Name,
                        Type = SearchResultType.Association,
                        AssocId = x.AssocId
                    }).ToList();
                foreach (var association in associations)
                {
                    association.Link = "/Organisation?name=" + HttpUtility.UrlEncode(association.Name) + "&id=" + Encryption.encrypt(association.AssocId.ToString());
                }
                return associations;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ProfileInfo> CheckUsersAreMembers(List<ProfileInfo> users, int assocId)
        {
            try
            {
                var association = _associationRepository.FindSingle(x => x.AssocId == assocId, x => x.Members);
                List<ProfileInfo> eligible = users;
                for (int i = 0; i < association.Members.Count; i++)
                {
                    for (int j = 0; j < users.Count; j++)
                    {
                        if (association.Members.ToList()[i].UserId == users[j].UserId)
                        {
                            eligible.Remove(users[j]);
                        }
                    }
                }
                return eligible;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public ScValidationResult InviteSelection(List<Guid> users, int assocId)
        {
            var result = new ScValidationResult();
            try
            {
                var association = _associationRepository.FindSingle(x => x.AssocId == assocId, c => c.Members);
                foreach (var userId in users)
                {
                    var user = _userRepository.FindSingle(x => x.UserId == userId, c => c.ProfileInfo);
                    association.Members.Add(new AssociationMembership
                    {
                        Joined = DateTime.Now,
                        User = user.ProfileInfo,
                        InviteResponse = RequestResponse.NotResponded,
                        Role = AssociationRole.Member,
                    });
                    _notificationRepository.Insert(new Notification
                    {
                        User = user,
                        LinkId = Encryption.encrypt(assocId.ToString()),
                        NotoficationType = NotoficationType.AssociationInvite,
                        Message = association.Name
                    });
                }
                _associationRepository.Update(association);
            }
            catch (Exception)
            {
                result.ValidationMessage = "Ett fel uppstod när inbjudning(ar) skulle skapas.";
            }
            return result;
        }

        public List<AssociationMembership> GetAssociationsForUser(string email)
        {
            var user = _userRepository.FindSingle(x => x.Email == email, c => c.ProfileInfo);

            var memberships = _associationMembershipRepository.Find(x => x.UserId == user.ProfileInfo.UserId, c => c.Association).ToList();
            return memberships;
        }
    }
}
