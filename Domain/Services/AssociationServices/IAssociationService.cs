﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Helpers;
using Domain.Models;

namespace Domain.Services.AssociationServices
{
    public interface IAssociationService
    {
        ScValidationResult CreateAssociation(string name, int vat, Image bannerImage, User associationCreator, AssociationType category, AssociationPrivacy privacyType);

        ScValidationResult EditAssociation(Association association);

        ScValidationResult AddMember(AssociationMembership member);

        Association GetAssociation(string name, int id);

        Association GetAssociationById(int id);

        List<AssociationMembership> GetInvites(Guid uid);

        ScValidationResult RespondToInvite(AssociationMembership invite, string email, int notificationId);

        List<AssociationSearchResult> FindAssociations(string query);

        List<ProfileInfo> CheckUsersAreMembers(List<ProfileInfo> users, int assocId);

        ScValidationResult InviteSelection(List<Guid> users, int assocId);

        List<AssociationMembership> GetAssociationsForUser(string email);
    }
}
