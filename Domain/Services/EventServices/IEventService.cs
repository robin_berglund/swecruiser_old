﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Helpers;
using Domain.Models;

namespace Domain.Services.EventServices
{
    public interface IEventService
    {
        ScValidationResult CreateEvent(string description, string name, DateTime starTime, DateTime endTime, EventType category, string creatorEmail);

        ScValidationResult EditEvent(Event theEvent);

        ScValidationResult InviteFriends(List<ProfileInfo> users, int eventId);

        ScValidationResult Rsvp(string participantEmail, Rsvp rsvp);

        Event GetEventById(int id);

        List<EventParticipation> GetEventsForUser(string userEmail);

        List<EventSearchResult> GetEvents(string queryString);

        List<ProfileInfo> GetFriendsViableForInvite(string inviterEmail, int eventId);

        ScValidationResult InviteSelection(List<Guid> users, int eventId);

        ScValidationResult RespondToInvite(EventParticipation invite, string email, int notificationId);
    }
}
