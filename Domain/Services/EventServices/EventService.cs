﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Helpers;
using Domain.Models;
using Domain.Services.Abstractions;
using Domain.Services.FriendServices;

namespace Domain.Services.EventServices
{
    public class EventService: IEventService
    {
        private IRepositoryService<Event> _eventRepository;
        private IRepositoryService<EventParticipation> _eventParticipationRepository; 
        private IRepositoryService<ProfileInfo> _profileRepository;
        private IRepositoryService<User> _userRepository;
        private IRepositoryService<Notification> _notificationRepository; 
        private IFriendService _friendService;

        public EventService(IRepositoryService<Event> eventRepository, IRepositoryService<EventParticipation> eventParticipationRepository, IRepositoryService<ProfileInfo> profileRepository, IRepositoryService<User> userRepository, IRepositoryService<Notification> notificationRepository, IFriendService friendService)
        {
            _eventRepository = eventRepository;
            _eventParticipationRepository = eventParticipationRepository;
            _profileRepository = profileRepository;
            _userRepository = userRepository;
            _notificationRepository = notificationRepository;
            _friendService = friendService;
        }

        public ScValidationResult CreateEvent(string description, string name, DateTime starTime, DateTime endTime, EventType category,
            string creatorEmail)
        {
            ScValidationResult result = new ScValidationResult();
            try
            {
                var creatorUser = _userRepository.FindSingle(x => x.Email == creatorEmail);
                Event ev = new Event
                {
                    Name = name,
                    Category = category,
                    Description = description,
                    StartTime = starTime,
                    EndTime = endTime,
                    CreationDate = DateTime.Now,
                };
                ev.Invites = new List<EventParticipation>();
                ev.Invites.Add(new EventParticipation
                {
                    UserId = creatorUser.UserId,
                    Role = EventRole.Creator,
                    Rsvp = Models.Rsvp.Attending,
                    EventId = ev.Id
                });
                _eventRepository.Insert(ev);
                result.IsValid = true;
            }
            catch (Exception)
            {
                result.ValidationMessage = "Ett fel inträffade när evenemanget skulle skapas";
            }
            return result;
        }

        public ScValidationResult EditEvent(Event theEvent)
        {
            ScValidationResult result = new ScValidationResult();
            try
            {
                _eventRepository.Update(theEvent);
                result.IsValid = true;
            }
            catch (Exception)
            {
                result.ValidationMessage = "Ett fel inträffade när evenemanget skulle uppdateras.";
            }
            return result;
        }

        public ScValidationResult InviteFriends(List<ProfileInfo> users, int eventId)
        {
            ScValidationResult result = new ScValidationResult();
            try
            {
                result.IsValid = true;
            }
            catch (Exception)
            {
                result.ValidationMessage = "Ett fel inträffade vid inbjudan till evenemanget.";
            }
            return result;
        }

        public ScValidationResult Rsvp(string participantEmail, Rsvp rsvp)
        {
            ScValidationResult result = new ScValidationResult();
            try
            {
                result.IsValid = true;
            }
            catch (Exception)
            {
                result.ValidationMessage = "Ett fel inträffade vid när närvarostatus till evenemanget skulle sparas.";
            }
            return result;
        }

        public Event GetEventById(int id)
        {
            try
            {
                return _eventRepository.FindSingle(x => x.Id == id, x => x.Invites);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<EventParticipation> GetEventsForUser(string userEmail)
        {
            try
            {
                List<EventParticipation> events = new List<EventParticipation>();
                var user = _userRepository.FindSingle(x => x.Email == userEmail);
                
                 events = _eventParticipationRepository.Find(x => x.UserId == user.UserId, x => x.Event)
                        .ToList();

                return events;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<EventSearchResult> GetEvents(string queryString)
        {
            try
            {
                return _eventRepository.Find(x => x.Name.Contains(queryString)).Select(x => new EventSearchResult
                {
                    EventType = x.Category,
                    Name = x.Name,
                    EventId = x.Id,
                    Type = SearchResultType.Event
                }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ProfileInfo> GetFriendsViableForInvite(string inviterEmail, int eventId)
        {
            try
            {
                List<ProfileInfo> friends = _friendService.GetFriendsForUser(inviterEmail);
                var theEvent = _eventRepository.FindSingle(x => x.Id == eventId, x => x.Invites);
                List<ProfileInfo> eligible = friends;
                for (int i = 0; i < theEvent.Invites.Count; i++)
                {
                    for (int j = 0; j < friends.Count; j++)
                    {
                        if (theEvent.Invites.ToList()[i].UserId == friends[j].UserId)
                        {
                            eligible.Remove(friends[j]);
                        }
                    }
                }
                return eligible;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public ScValidationResult InviteSelection(List<Guid> users, int eventId)
        {
            var result = new ScValidationResult();
            try
            {
                var theEvent = _eventRepository.FindSingle(x => x.Id == eventId, c => c.Invites);
                foreach (var userId in users)
                {
                    var user = _userRepository.FindSingle(x => x.UserId == userId, c => c.ProfileInfo);
                    theEvent.Invites.Add(new EventParticipation
                    {
                        InvitedUser = user.ProfileInfo,
                        Rsvp = Models.Rsvp.NotResponded,
                        Role = EventRole.Participator,
                        
                    });
                    _notificationRepository.Insert(new Notification
                    {
                        User = user,
                        LinkId = Encryption.encrypt(eventId.ToString()),
                        NotoficationType = NotoficationType.EventInvite,
                        Message = theEvent.Name
                    });
                }
                _eventRepository.Update(theEvent);
            }
            catch (Exception)
            {
                result.ValidationMessage = "Ett fel uppstod när inbjudning(ar) skulle skapas.";
            }
            return result;
        }

        public ScValidationResult RespondToInvite(EventParticipation invite, string email, int notificationId)
        {
            ScValidationResult result = new ScValidationResult();
            var response = invite.Rsvp;
            try
            {
                var user = _userRepository.FindSingle(x => x.Email == email);
                invite =
                    _eventParticipationRepository.FindSingle(
                        x => x.EventId == invite.EventId && x.UserId == user.UserId, x => x.Event);
            }
            catch (Exception)
            {
                result.ValidationMessage = "Ett fel uppstod vid svar på inbjudan.";
            }
            if (response == Models.Rsvp.Attending)
            {
                try
                {
                    invite.Rsvp = Models.Rsvp.Attending;
                    var notification = _notificationRepository.FindSingle(x => x.Id == notificationId);
                    notification.NotoficationType = NotoficationType.Info;
                    notification.Message = "Du har tackat ja till att delta i " + invite.Event.Name;
                    _notificationRepository.Update(notification);
                    _eventParticipationRepository.Update(invite);
                    result.IsValid = true;
                    result.ValidationMessage = "Inbjudan accepterad.";
                }
                catch (Exception)
                {
                    result.ValidationMessage = "Något gick fel vid bekräftelse av inbjudan.";
                }
            }
            else if (response == Models.Rsvp.NotAttending)
            {
                try
                {
                    var notification = _notificationRepository.FindSingle(x => x.Id == notificationId);
                    _notificationRepository.Delete(notification);
                    _eventParticipationRepository.Delete(invite);
                    result.IsValid = true;
                    result.ValidationMessage = "Inbjudan borttagen";

                }
                catch (Exception)
                {
                    result.ValidationMessage = "Något gick fel vid nekan av inbjudan";
                }
            }

            return result;
        }
    }
}
