﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services.PasswordHandler
{
    public interface IPasswordHandler
    {
        string HashPassword(string password);

        bool ValidatePassword(string password, string correctHash);
    }
}
