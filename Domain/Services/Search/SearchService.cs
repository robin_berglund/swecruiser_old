﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Models;
using Domain.Services.Abstractions;

namespace Domain.Services.Search
{
    public class SearchService : ISearchService
    {
        IRepositoryService<ProfileInfo> _profileRepository;
        public SearchService(IRepositoryService<ProfileInfo> profileRepository)
        {
            _profileRepository = profileRepository;
        }
        public List<SearchResult> GetTopTenContaining(string queryString)
        {
            List<ProfileInfo> users = _profileRepository
                .Find(x => x.Firstname.Contains(queryString) 
            || x.Lastname.Contains(queryString) 
            || x.City.Contains(queryString))
            .Take(10).ToList();

            List<SearchResult> results = new List<SearchResult>();

            foreach (var user in users)
            {
                SearchResult result = new SearchResult();
                result.Name = user.Firstname + " " + user.Lastname;
                result.Link = user.ProfileUrl;
                result.Type = SearchResultType.User;
                results.Add(result);
            }
            return results;
        }
    }
}
