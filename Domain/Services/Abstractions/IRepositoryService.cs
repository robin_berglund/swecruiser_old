﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services.Abstractions
{
    public interface IRepositoryService<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        TEntity GetById(object id);
        TEntity Insert(TEntity entity);

        IQueryable<TEntity> InsertMany(ICollection<TEntity> entities);
        TEntity Update(TEntity entity);
        IQueryable<TEntity> UpdateMany(ICollection<TEntity> entities);
        void Delete(TEntity entity);

        TEntity FindSingle(Expression<Func<TEntity, bool>> predicate = null, params Expression<Func<TEntity, object>>[] includes);
        IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate = null, params Expression<Func<TEntity, object>>[] includes);
        IQueryable<TEntity> FindIncluding(params Expression<Func<TEntity, object>>[] includeProperties);

        IQueryable<TEntity> Include(params System.Linq.Expressions.Expression<Func<TEntity, object>>[] includes);
        int Count(Expression<Func<TEntity, bool>> predicate = null);
        bool Exist(Expression<Func<TEntity, bool>> predicate = null);


    }
}
