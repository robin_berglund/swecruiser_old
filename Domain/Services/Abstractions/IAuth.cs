﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Models;

namespace Domain.Services.Abstractions
{
    public interface IAuth
    {
        void DoAuth(User user, bool persistLogin, string permissions);

        void Logout();
    }
}
