﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Helpers;
using Domain.Models;
using Domain.Services.Abstractions;

namespace Domain.Services.FriendServices
{
    public class FriendService : IFriendService
    {
        private IRepositoryService<FriendRequest> _friendRequestRepository;
        private IRepositoryService<User> _userRepository;
        private IRepositoryService<FriendShip> _friendShipRepository; 

        public FriendService(IRepositoryService<User> userRepository, IRepositoryService<FriendRequest> friendRequestRepository, IRepositoryService<FriendShip> friendShipRepository)
        {
            _friendRequestRepository = friendRequestRepository;
            _userRepository = userRepository;
            _friendShipRepository = friendShipRepository;
        }


        public RequestResponse GetFriendStatus(User userToCheck, string visiterEmail)
        {
            RequestResponse response = new RequestResponse();
            try
            {

                User visitor = _userRepository.FindSingle(x => x.Email == visiterEmail, x => x.Friends);
                var request = userToCheck.FriendRequests.FirstOrDefault(x => x.Requester == visitor);
                if (visitor.Friends.Any(x => x.Friend == userToCheck.ProfileInfo) && userToCheck.Friends.Any(x => x.Friend == visitor.ProfileInfo))
                {
                    response = RequestResponse.Accepted;
                }

                else if (request != null && request.Response == RequestResponse.NotResponded)
                {
                    response = RequestResponse.NotResponded;
                }
                else if (request == null)
                {
                    response = RequestResponse.NotRequested;
                }

            }
            catch (Exception)
            {
                throw;
            }
            return response;

        }

        public bool SendFriendRequest(Guid userId, string requesterEmail)
        {
            try
            {
                var requestedUser = _userRepository.FindSingle(x => x.UserId == userId, c => c.FriendRequests);
                var requester = _userRepository.FindSingle(x => x.Email == requesterEmail, x => x.ProfileInfo);
                FriendRequest request = new FriendRequest
                {
                    Requester = requester,
                    IsRead = false,
                    Response = RequestResponse.NotResponded,
                    RequesterLink = @"<a href=""Profile?uid=" + requester.UserId + @""">" + requester.ProfileInfo.Firstname + " " + requester.ProfileInfo.Lastname + "</a>"
                };
                requestedUser.FriendRequests.Add(request);
                _userRepository.Update(requester);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public ScValidationResult RespondToFriendRequest(RequestResponse response, int requestId, string email)
        {
            ScValidationResult result = new ScValidationResult();
         
            FriendRequest request = _friendRequestRepository.FindSingle(x => x.Id == requestId, x => x.User);
            var u1 = _userRepository.FindSingle(x => x.UserId == request.User.UserId, c => c.ProfileInfo, c => c.Friends, c => c.FriendRequests);
            if (u1.Email != email)
            {
                result.ValidationMessage = "Du har inte tillgång till den här vänförfrågningen.";
                return result;
            }
            if (response == RequestResponse.Accepted)
            {
                try
                {
                    
                    var u2 = _userRepository.FindSingle(x => x.UserId == request.Requester.UserId, c => c.ProfileInfo, c => c.Friends, c => c.Notifications);
                    if (u2.Friends == null)
                    {
                        u2.Friends = new List<FriendShip>();
                    }
                    if (u1.Friends == null)
                    {
                        u1.Friends = new List<FriendShip>();
                    }
                    u2.Friends.Add(new FriendShip { Since = DateTime.Now, Friend = u1.ProfileInfo });
                    u2.Notifications.Add(new Notification { Message = u1.ProfileInfo.Firstname + " " + u1.ProfileInfo.Lastname +  " och du är nu polare!"});
                    u1.Friends.Add(new FriendShip { Since = DateTime.Now, Friend = u2.ProfileInfo });
                    _userRepository.Update(u2);
                    _friendRequestRepository.Delete(request);
                    result.IsValid = true;
                    result.ValidationMessage = "Du och " + u2.ProfileInfo.Firstname + " är nu vänner!";

                }
                catch (Exception)
                {
                    result.ValidationMessage = "Ett fel uppstod när vänskapsband skulle knytas. Försök igen om en stund.";
                }
            }
            else if (request.Response == RequestResponse.Denied)
            {
                try
                {
                    u1.FriendRequests.Remove(request);
                    _userRepository.Update(u1);
                    result.IsValid = true;
                    result.ValidationMessage = "Vänskapsförfrågan mackulerad";
                }
                catch (Exception)
                {
                    result.ValidationMessage = "Ett fel uppstod när vänförfrågan skulle raderas. Försök igen om en stund";
                }
            }
            return result;
        }

        public List<ProfileInfo> GetFriendsForUser(string email)
        {
            try
            {
                return _friendShipRepository.Find(x => x.User.Email == email).Select(x => x.Friend).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            
        }
    }
}
