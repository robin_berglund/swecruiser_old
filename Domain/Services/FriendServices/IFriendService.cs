﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Helpers;
using Domain.Models;

namespace Domain.Services.FriendServices
{
    public interface IFriendService
    {
        RequestResponse GetFriendStatus(User userToCheck, string visiterEmail);

        bool SendFriendRequest(Guid userId, string requesterEmail);

        ScValidationResult RespondToFriendRequest(RequestResponse response, int requestId, string email);

        List<ProfileInfo> GetFriendsForUser(string email);
    }
}
