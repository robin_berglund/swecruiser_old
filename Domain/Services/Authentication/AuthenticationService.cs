﻿using Domain.Helpers;
using Domain.Models;
using Domain.Services.Abstractions;
using Domain.Services.Authentication;
using Domain.Services.PasswordHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services.Authentication
{
    public class AuthenticationService : IAuthenticationService
    {
        private IRepositoryService<User> _userRepository;
        private IRepositoryService<UserRole> _roleRepository;
        private IPasswordHandler _passwordHandler;
        private IAuth _auth;
        private IMailer _mailer;

        public AuthenticationService(IRepositoryService<User> userRepository, IRepositoryService<UserRole> roleRepository,
            IPasswordHandler passwordHandler, IAuth auth, IMailer mailer)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _passwordHandler = passwordHandler;
            _auth = auth;
            _mailer = mailer;
        }

        public ScValidationResult Login(string email, string password, bool rememberLogin)
        {
            ScValidationResult result = new ScValidationResult();
            var user = _userRepository.FindSingle(x => x.Email == email);

            if (user != null &&
                _passwordHandler.ValidatePassword(
                    password,  user.PasswordHash))
            {
                string permissions = ImplodePermissions(user.Role.Permissions.ToList());
                _auth.DoAuth(user, rememberLogin, permissions);
                result.IsValid = true;
                return result;
            }
            result.ValidationMessage = "Felaktig e-postadress eller lösenord.";
            return result;
        }

        private string ImplodePermissions(List<Permission> permissions)
        {
            return string.Join(",", permissions.Select(x => x.Description));
        }
        public void Logout()
        {
            _auth.Logout();
        }

        public ScValidationResult RegisterUser(string email, string password)
        {
            var validation = new ScValidationResult();
            if (_userRepository.FindSingle(x => x.Email == email) == null)
            {
                User user = new User();
                user.PasswordHash =  _passwordHandler.HashPassword(password);
                user.Email = email;
                user.Role = _roleRepository.FindSingle(x => x.Description == "Unconfirmedmember");
                user.AccountConfirmationToken = Guid.NewGuid();
                try
                {
                    User registeredUser = _userRepository.Insert(user);

                    user.ProfileInfo = new ProfileInfo { ProfileUrl = user.UserId.ToString() };

                    _userRepository.Update(user);
                    _mailer.SendConfirmationMail(user);
                    validation.IsValid = true;
                    return validation;
                }
                catch (Exception ex)
                {
                    validation.ValidationMessage = "Ett fel inträffade vid registreringen. Prova igen om en stund.";
                }
                
            }
            else
            {
                validation.ValidationMessage = "Den angivna e-postadressen är redan registrerad";
            }
            
            return validation; 
        }

        public User AuthenticateRequest(System.Web.HttpContextBase context)
        {
            if (context.Request.IsAuthenticated)
            {
                var user = _userRepository.FindSingle(x => x.Email == context.User.Identity.Name);

                if (user != null)
                {
                    context.User = user;
                    return user;
                }
            }

            return null;
        }

        public ScValidationResult ConfirmUser(Guid userID, Guid confirmationToken, bool isPersistent)
        {
            ScValidationResult result = new ScValidationResult();
            var user = _userRepository.FindSingle(x => x.UserId == userID);
            if (user != null && !user.IsConfirmedAccount)
            {
                if (user.AccountConfirmationToken == confirmationToken)
                {
                    user.IsConfirmedAccount = true;
                    user.Role = _roleRepository.FindSingle(x => x.Description == "ConfirmedMember");
                    _userRepository.Update(user);
                    result.IsValid = true;
                    string permissions = ImplodePermissions(user.Role.Permissions.ToList());
                    _auth.DoAuth(user, isPersistent, permissions);
                    return result;
                }
            }
            else if (user == null)
            {
                result.ValidationMessage = "Kunde inte hitta användare!";
            }
            else if(user.IsConfirmedAccount)
            {
                result.ValidationMessage = "Det här kontot är redan bekräftat!";
            }
            return result;
        }

        public ScValidationResult ValidatePwResetUrl(Guid userID, Guid pwrToken)
        {
            var result = new ScValidationResult();
            var user = _userRepository.FindSingle(x => x.UserId == userID);

            if(user == null)
            {
                result.ValidationMessage = "Kunde inte hitta användare.";
            }
            else if (user.PasswordRecoveryToken != pwrToken)
            {
                result.ValidationMessage = "Länken är felaktig. Prova att klicka på länken i din inkorg igen. Om problemet kvarstår, prova att återställa lösenordet igen eller kontakta oss.";
            }
            else if(user.PasswordRecoveryDeadline < DateTime.Now)
            {
                result.ValidationMessage = "Den här länken är inte längre giltig. Var god återställ ditt lösenord på nytt.";
            }
            else
            {
                result.IsValid = true;
            }
            return result;
        }

        public ScValidationResult ResetPassword(Guid userID, Guid pwrToken, string password)
        {
            var result = new ScValidationResult();
            var user = _userRepository.FindSingle(x => x.UserId == userID);

            if (user == null)
            {
                result.ValidationMessage = "Kunde inte hitta användare.";
            }
            else if (user.PasswordRecoveryToken != pwrToken)
            {
                result.ValidationMessage = "Länken är felaktig. Prova att klicka på länken i din inkorg igen. Om problemet kvarstår, prova att återställa lösenordet igen eller kontakta oss.";
            }
            else
            {
                try
                {
                    user.PasswordHash = _passwordHandler.HashPassword(password);
                    _userRepository.Update(user);
                    result.IsValid = true;
                }
                catch (Exception)
                {
                    result.ValidationMessage = "Ett okänt fel uppstod när lösenordet skulle uppdateras. Var god försök igen om en stund";
                }                

            }
            return result;
        }

        public ScValidationResult SendPasswordResetEmail(string email)
        {
            var result = new ScValidationResult();
            var user = _userRepository.FindSingle(x => x.Email == email);
            try
            {
                user.PasswordRecoveryDeadline = DateTime.Now.AddDays(10);
                user.PasswordRecoveryToken = Guid.NewGuid();
                _mailer.SendPasswordResetMail(user);
                _userRepository.Update(user);
                result.IsValid = true;
            }
            catch (Exception)
            {
                result.ValidationMessage = "Ett fel uppstod. Kontrollera e-postadressen och försök igen.";
            }
            return result;
        }
    }
}
