﻿using Domain.Helpers;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Domain.Services.Authentication
{
    public interface IAuthenticationService
    {
        ScValidationResult Login(string username, string password, bool rememberLogin);

        void Logout();

        ScValidationResult RegisterUser(string username, string password);        

        User AuthenticateRequest(HttpContextBase context);

        ScValidationResult ConfirmUser(Guid userID, Guid confirmationToken, bool isPersistent);

        ScValidationResult ValidatePwResetUrl(Guid userID, Guid pwrToken);

        ScValidationResult ResetPassword(Guid userID, Guid pwrToken, string password);

        ScValidationResult SendPasswordResetEmail(string email);
    }
}
