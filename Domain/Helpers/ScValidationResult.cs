﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Helpers
{
    public class ScValidationResult
    {
        public bool IsValid { get; set; }

        public string ValidationMessage { get; set; }
    }
}
