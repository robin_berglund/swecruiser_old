﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Domain.Helpers
{
    public static class Encryption
    {
        public static string encrypt(string ToEncrypt)
        {
            byte[] clearTextBytes = Encoding.UTF8.GetBytes(ToEncrypt);
            System.Security.Cryptography.SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();
            MemoryStream ms = new MemoryStream();

            string key1 = "a80aaaaa12aaaa23";
            string key2 = "aaaaa52aa8aaa12aaaaa76aaa53aaaaa";

            byte[] rgbIV = Encoding.ASCII.GetBytes(key1);
            byte[] key = Encoding.ASCII.GetBytes(key2);
            CryptoStream cs = new CryptoStream(ms, rijn.CreateEncryptor(key, rgbIV), CryptoStreamMode.Write);
            cs.Write(clearTextBytes, 0, clearTextBytes.Length);
            cs.Close();

            string encrypted = Convert.ToBase64String(ms.ToArray());
            return encrypted;
        }
        public static string decrypt(string cypherString)
        {
            cypherString = cypherString.Replace(' ', '+');
            byte[] encryptedTextBytes = Convert.FromBase64String(cypherString);
            MemoryStream ms = new MemoryStream();
            System.Security.Cryptography.SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();

            string key1 = "a80aaaaa12aaaa23";
            string key2 = "aaaaa52aa8aaa12aaaaa76aaa53aaaaa";

            byte[] rgbIV = Encoding.ASCII.GetBytes(key1);
            byte[] key = Encoding.ASCII.GetBytes(key2);
            CryptoStream cs = new CryptoStream(ms, rijn.CreateDecryptor(key, rgbIV), CryptoStreamMode.Write);
            cs.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);
            cs.Close();
            return Encoding.UTF8.GetString(ms.ToArray());
        }
}
}