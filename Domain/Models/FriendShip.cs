﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class FriendShip
    {
        public Guid UserId { get; set; }

        public Guid FriendId { get; set; }

        public User User { get; set; }

        public virtual ProfileInfo Friend { get; set; }

        public DateTime Since { get; set; }
    }
}
