﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Event
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public virtual Image BannerImage { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }
        
        public ICollection<EventParticipation> Invites { get; set; }

        public ICollection<EventEntry> WallEntries { get; set; }

        public virtual Association OwnerAssociation { get; set; }

        public ICollection<Association> InvitedAssociations { get; set; }

        public EventType Category { get; set; }


    }
}
