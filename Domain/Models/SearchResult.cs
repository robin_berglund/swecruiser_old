﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class SearchResult
    {
        public string Link { get; set; }

        public string Name { get; set; }

        public SearchResultType Type { get; set; }
    }
}
