﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class VehicleBio
    {
        public int Id { get; set; }

        public string Model { get; set; }

        public int Year { get; set; }

        public ICollection<Image> Images { get; set; }

        public string Description { get; set; }

    }
}
