﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class AssociationMembership
    {
        [Key, Column(Order = 0), ForeignKey("User")]
        public Guid UserId { get; set; }

        [Key, Column(Order = 1)]
        public int AssocId { get; set; }
     
        public virtual ProfileInfo User { get; set; }        
        
        public Association Association { get; set; }

        public DateTime Joined { get; set; }

        public AssociationRole Role { get; set; }

        public RequestResponse InviteResponse { get; set; }
    }
}
