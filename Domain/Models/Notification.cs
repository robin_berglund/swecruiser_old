﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Notification
    {
        public int Id { get; set; }

        public Guid UserId { get; set; }

        public User User { get; set; }
        public bool IsRead { get; set; }

        public string Message { get; set; }

        public NotoficationType NotoficationType { get; set; }

        public string LinkId { get; set; }
      
    }
}
