﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class FriendRequest
    {
        public int Id { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        public virtual User Requester { get; set; }

        public RequestResponse Response { get; set; }

        public bool IsRead { get; set; }

        public string RequesterLink { get; set; }

    }
}
