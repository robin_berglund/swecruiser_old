﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class EventParticipation
    {
       
        [Key, Column(Order = 0)]
        public Guid UserId { get; set; }
        
        [Key, Column(Order = 1)]
        public int EventId { get; set; }

        public Rsvp Rsvp { get; set; }

        public virtual ProfileInfo InvitedUser { get; set; }

        public bool IsInvitedByUser { get; set; }
        
        public Association InvitedByAssoc { get; set; }
        
        public Guid InviterUserId { get; set; }

        public User InvitedByUser { get; set; }

        public Event Event { get; set; }

        public EventRole Role { get; set; }
    }
}
