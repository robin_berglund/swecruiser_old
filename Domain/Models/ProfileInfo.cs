﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class ProfileInfo
    {
       
        [Key]
        [ForeignKey("User")]
        public Guid UserId { get; set; }

        public User User { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string City { get; set; }

        public string FavouriteVehicles { get; set; }

        public ICollection<VehicleBio> OwnedVehicles { get; set; }

        public virtual Image ProfileImage { get; set; }
            
        public string ProfileUrl { get; set; }
        
        

    }
}
