﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class AssociationSearchResult : SearchResult
    {
        public AssociationType AssociationType { get; set; }

        public int AssocId { get; set; }
    }
}
