﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class AssociationEntry : Entry
    {
        public int AssocId { get; set; }

        public Association Association { get; set; }
    }
}
