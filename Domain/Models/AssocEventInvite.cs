﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class AssocEventInvite
    {
        public int Id { get; set; }

        public Rsvp Rsvp { get; set; }

        public virtual Event Event { get; set; }

        public ICollection<EventParticipation> InvitedMembers { get; set; }
    }
}
