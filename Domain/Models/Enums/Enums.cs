﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public enum RequestResponse { Accepted, Denied, NotResponded, NotRequested };

    public enum Rsvp { Attending, MaybeAttending, NotAttending, NotResponded };

    public enum EventType { Meetup, Market, Cruising, Race}

    public enum AssociationType
    {
        [Display(Name = "Organisation")]
        Organisation,
        [Display(Name = "Sällskap")]
        Association,
        [Display(Name = "Återförsäljare")]
        Retail
    }

    public enum AssociationRole
    {
        [Display(Name = "Ägare")]
        Owner,
        [Display(Name = "Admin")]
        Admin,
        [Display(Name = "Medlem")]
        Member,
        [Display(Name = "Följare")]
        Follower,
        NoMember
    }

    public enum EventRole
    {
        [Display(Name = "Skapare")]
        Creator,
        [Display(Name = "Admin")]
        Admin,
        [Display(Name = "Deltagare")]
        Participator,
        NotAccepted,
        NotInvited
    }

    public enum AssociationPrivacy { Private, Public, Secret }

    public enum EventPrivacy { Private, Public }

    public enum SearchResultType { User, Event, Association }

    public enum ReportCategories { Inappropriate, Hateful, Spam, Other }

    public enum NotoficationType { FriendResponse, AssociationInvite, EventInvite, ProfileEntry,AssociationEntry, Info }
    class Enums
    {
    }
}
