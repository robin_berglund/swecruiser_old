﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class EventEntry : Entry
    {

        public int EventId { get; set; }

        public Event Event { get; set; }
    }
}
