﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Association
    {
        [Key]
        public int AssocId { get; set; }

        public string Name { get; set; }

        public int Vat { get; set; }

        public virtual Image BannerImage { get; set; }

        public virtual User AssociationOwner { get; set; }

        public ICollection<AssociationMembership> Members { get; set; }
        public AssociationPrivacy PrivacyType { get; set; }        

        public ICollection<Event> HostedEvents { get; set; }

        public ICollection<EventParticipation> EventInvites { get; set; }

        public ICollection<AssociationEntry> WallEntries { get; set; }

        public AssociationType Category { get; set; }
    }
}
