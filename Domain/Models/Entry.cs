﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Entry
    {

        public int Id { get; set; }
        public Guid AuthorId { get; set; }
        public User Author { get; set; }

        public bool IsPrivate { get; set; }
        public DateTime PublishDate { get; set; }

        public string Message { get; set; }

    }
}
