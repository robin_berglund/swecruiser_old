﻿using Domain.Models.BaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class EntryReport : Report
    {
        public Entry ReportedEntry { get; set; }
    }
}
