﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class User : IPrincipal
    {        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserId { get; set; }

        public string Email { get; set; }
     
        public ProfileInfo ProfileInfo { get; set; }

        public bool IsConfirmedAccount { get; set; }
        
        public Guid AccountConfirmationToken { get; set; }

        public string PasswordHash { get; set; }

        public bool IsPermanentlyBanned { get; set; }

        public Guid SecurityStamp { get; set; }

        public Guid PasswordRecoveryToken { get; set; }

        public DateTime? PasswordRecoveryDeadline { get; set; }

        public ICollection<FriendShip> Friends { get; set; }

        public ICollection<AssociationMembership> Memberships { get; set; }

        public ICollection<Notification> Notifications { get; set; }

        public ICollection<FriendRequest> FriendRequests { get; set; }

        public ICollection<UserEntry> ProfileEntries { get; set; }

        public ICollection<User> BlockedUsers { get; set; }

        public ICollection<EventParticipation> EventInvites { get; set; }

        public virtual UserRole Role { get; set; }

        public IIdentity Identity
        {
            get
            {
                return new GenericIdentity(Email);
            }
        }

        public bool IsInRole(string role)
        {
            return Role != null && Role.Permissions.Any(x => x.Description == role);
        }
    }
}
