﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Permission
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public ICollection<UserRole> Roles { get; set; }
    }
}
