﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.BaseModels
{
    public abstract class Report
    {
        public int Id { get; set; }

        public DateTime ReportDate { get; set; }

        public User ReportedByUser { get; set; }

        public ReportCategories ReasonOfReport { get; set; }

        public string AdditionalInfo { get; set; }

    }
}
