﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class EventSearchResult : SearchResult
    {
        public EventType EventType { get; set; }

        public int EventId { get; set; }
    }
}
